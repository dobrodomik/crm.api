package api

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/dobrodomik/crm.api/internal/repository"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"net/http"
)

type project struct {
	*Api
}

type UpdateProjectRequest struct {
	Uuid string `param:"id" validate:"required,uuid"`
	*repository.Project
}

func newProjectRoute(api *Api) *project {
	route := &project{
		Api: api,
	}

	return route
}

func (m *project) Route(groups *groups) {
	groups.authGroup.POST("/projects", m.createProject)
	groups.authGroup.PUT("/projects/:id", m.updateProject)
	groups.authGroup.GET("/projects", m.listProjects)
	groups.authGroup.GET("/projects/:id", m.getProjectByUuId)
	groups.authGroup.DELETE("/projects/:id", m.deleteProject)

	groups.noAuthGroup.GET("/projects", m.listProjects)
	groups.noAuthGroup.GET("/projects/:id", m.getProjectByUuId)
}

func (m *project) createProject(c echo.Context) error {
	req := new(repository.Project)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	obj, err := m.repository.GetProjectRepository().CreateProject(ctx, req)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusCreated, obj)
}

func (m *project) updateProject(c echo.Context) error {
	req := new(UpdateProjectRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	obj, err := m.repository.GetProjectRepository().UpdateProject(ctx, req.Uuid, req.Project)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, obj)
}

func (m *project) getProjectByUuId(c echo.Context) error {
	req := new(GetByUuIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	obj, err := m.repository.GetProjectRepository().GetProjectByUuid(ctx, req.Uuid)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	return c.JSON(http.StatusOK, obj)
}

func (m *project) listProjects(c echo.Context) error {
	req := new(repository.ProjectFilter)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	res, err := m.repository.GetProjectRepository().ListProject(c.Request().Context(), req)

	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, res)
}

func (m *project) deleteProject(c echo.Context) error {
	req := new(GetByUuIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	user, err := m.repository.GetProjectRepository().GetProjectByUuid(ctx, req.Uuid)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	err = m.repository.GetProjectRepository().DeleteProject(ctx, req.Uuid)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	return c.JSON(http.StatusOK, user)
}
