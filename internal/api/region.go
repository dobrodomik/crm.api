package api

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/dobrodomik/crm.api/internal/repository"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"net/http"
	"strconv"
	"strings"
)

type region struct {
	*Api
}

type UpdateRegionRequest struct {
	Id int64 `param:"id" validate:"required,numeric,gt=0"`
	*repository.Region
}

func newRegionRoute(api *Api) *region {
	route := &region{
		Api: api,
	}

	return route
}

func (m *region) Route(groups *groups) {
	groups.authGroup.POST("/regions", m.createRegion)
	groups.authGroup.PUT("/regions/:id", m.updateRegion)
	groups.authGroup.GET("/regions", m.listRegions)
	groups.authGroup.GET("/regions/:id", m.getRegionById)
	groups.authGroup.DELETE("/regions/:id", m.deleteRegion)
}

func (m *region) createRegion(c echo.Context) error {
	req := new(repository.Region)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	coords, err := m.findMapCoordsByAddress(req.Name)

	if err != nil {
		return err
	}

	coordsSlice := strings.Split(coords, " ")
	req.CenterCoords = []interface{}{coordsSlice[1], coordsSlice[0]}

	ctx := c.Request().Context()
	obj, err := m.repository.GetRegionRepository().CreateRegion(ctx, req)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusCreated, obj)
}

func (m *region) updateRegion(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		return c.JSON(http.StatusBadRequest, pkg.ErrorInvalidRequest.SetDetails(err.Error()))
	}

	req := new(repository.Region)
	err = m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	obj, err := m.repository.GetRegionRepository().UpdateRegion(ctx, int64(id), req)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, obj)
}

func (m *region) getRegionById(c echo.Context) error {
	req := new(GetByIdRequestInt64)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	obj, err := m.repository.GetRegionRepository().GetRegionById(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	return c.JSON(http.StatusOK, obj)
}

func (m *region) listRegions(c echo.Context) error {
	req := new(repository.RegionFilter)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	res, err := m.repository.GetRegionRepository().ListRegions(c.Request().Context(), req)

	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, res)
}

func (m *region) deleteRegion(c echo.Context) error {
	req := new(GetByIdRequestInt64)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	user, err := m.repository.GetRegionRepository().GetRegionById(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	err = m.repository.GetRegionRepository().DeleteRegion(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	return c.JSON(http.StatusOK, user)
}
