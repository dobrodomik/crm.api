package api

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/dobrodomik/crm.api/internal/repository"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"net/http"
)

type partner struct {
	*Api
}

type UpdatePartnerRequest struct {
	Uuid string `param:"id" validate:"required,uuid"`
	*repository.Partner
}

func newPartnerRoute(api *Api) *partner {
	route := &partner{
		Api: api,
	}

	return route
}

func (m *partner) Route(groups *groups) {
	groups.authGroup.POST("/partners", m.createPartner)
	groups.authGroup.PUT("/partners/:id", m.updatePartner)
	groups.authGroup.GET("/partners", m.listPartners)
	groups.authGroup.GET("/partners/:id", m.getPartnerByUuId)
	groups.authGroup.DELETE("/partners/:id", m.deletePartner)

	groups.noAuthGroup.GET("/partners", m.listPartners)
	groups.noAuthGroup.GET("/partners/:id", m.getPartnerByUuId)
}

func (m *partner) createPartner(c echo.Context) error {
	req := new(repository.Partner)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	obj, err := m.repository.GetPartnerRepository().CreatePartner(ctx, req)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusCreated, obj)
}

func (m *partner) updatePartner(c echo.Context) error {
	req := new(UpdatePartnerRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	obj, err := m.repository.GetPartnerRepository().UpdatePartner(ctx, req.Uuid, req.Partner)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, obj)
}

func (m *partner) getPartnerByUuId(c echo.Context) error {
	req := new(GetByUuIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	obj, err := m.repository.GetPartnerRepository().GetPartnerByUuid(ctx, req.Uuid)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	return c.JSON(http.StatusOK, obj)
}

func (m *partner) listPartners(c echo.Context) error {
	req := new(repository.PartnerFilter)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	res, err := m.repository.GetPartnerRepository().ListPartners(c.Request().Context(), req)

	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, res)
}

func (m *partner) deletePartner(c echo.Context) error {
	req := new(GetByUuIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	user, err := m.repository.GetPartnerRepository().GetPartnerByUuid(ctx, req.Uuid)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	err = m.repository.GetPartnerRepository().DeletePartners(ctx, req.Uuid)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	return c.JSON(http.StatusOK, user)
}
