package api

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/dobrodomik/crm.api/internal/repository"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"net/http"
)

type news struct {
	*Api
}

type UpdateNewsRequest struct {
	Uuid string `param:"id" validate:"required,uuid"`
	*repository.News
}

func newNewsRoute(api *Api) *news {
	route := &news{
		Api: api,
	}

	return route
}

func (m *news) Route(groups *groups) {
	groups.authGroup.POST("/news", m.createNews)
	groups.authGroup.PUT("/news/:id", m.updateNews)
	groups.authGroup.GET("/news", m.listNews)
	groups.authGroup.GET("/news/:id", m.getNewsByUuId)
	groups.authGroup.DELETE("/news/:id", m.deleteNews)

	groups.noAuthGroup.GET("/news", m.listNews)
	groups.noAuthGroup.GET("/news/:id", m.getNewsByUuId)
}

func (m *news) createNews(c echo.Context) error {
	req := new(repository.News)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	obj, err := m.repository.GetNewsRepository().CreateNews(ctx, req)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusCreated, obj)
}

func (m *news) updateNews(c echo.Context) error {
	req := new(UpdateNewsRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	obj, err := m.repository.GetNewsRepository().UpdateNews(ctx, req.Uuid, req.News)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, obj)
}

func (m *news) getNewsByUuId(c echo.Context) error {
	req := new(GetByUuIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	obj, err := m.repository.GetNewsRepository().GetNewsByUuid(ctx, req.Uuid)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	return c.JSON(http.StatusOK, obj)
}

func (m *news) listNews(c echo.Context) error {
	req := new(repository.NewsFilter)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	res, err := m.repository.GetNewsRepository().ListNews(c.Request().Context(), req)

	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, res)
}

func (m *news) deleteNews(c echo.Context) error {
	req := new(GetByUuIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	user, err := m.repository.GetNewsRepository().GetNewsByUuid(ctx, req.Uuid)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	err = m.repository.GetNewsRepository().DeleteNews(ctx, req.Uuid)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	return c.JSON(http.StatusOK, user)
}
