package api

import (
	"encoding/json"
	"errors"
	"github.com/bxcodec/faker/v3"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/dobrodomik/crm.api/internal/config"
	"gitlab.com/dobrodomik/crm.api/internal/mocks"
	"gitlab.com/dobrodomik/crm.api/internal/repository"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest/observer"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

type UserApiTestSuite struct {
	suite.Suite
	logObserver *zap.Logger
	zapRecorder *observer.ObservedLogs
	api         *Api
	user        *repository.User
}

func Test_UserApi(t *testing.T) {
	suite.Run(t, new(UserApiTestSuite))
}

func (suite *UserApiTestSuite) SetupTest() {
	cfg := config.NewConfig(nil)

	lvl := zap.NewAtomicLevel()
	core, zapRecorder := observer.New(lvl)
	logger := zap.New(core)

	user := &repository.User{}
	err := faker.FakeData(user)

	if err != nil {
		suite.FailNow("Fake user generation failed", "%v", err)
	}

	user.Role = []interface{}{pkg.RoleAdministrator, pkg.RoleRegionCurator}

	userRepositoryMock := &mocks.UserRepositoryInterface{}
	userRepositoryMock.On("AuthUser", mock.Anything, mock.Anything, mock.Anything).Return(user, nil)
	userRepositoryMock.On("GetUserById", mock.Anything, mock.Anything).Return(user, nil)
	userRepositoryMock.On("ListUsers", mock.Anything, mock.Anything).
		Return(
			&repository.Paginate{
				Count: 2,
				Items: []*repository.User{user, user},
			},
			nil,
		)
	userRepositoryMock.On("CreateUser", mock.Anything, mock.Anything).Return(user, nil)
	userRepositoryMock.On("UpdateUser", mock.Anything, mock.Anything, mock.Anything).Return(user, nil)
	userRepositoryMock.On("HashPassword", mock.Anything).Return("password", nil)
	repositoryMock := &mocks.Repository{}
	repositoryMock.On("GetUserRepository").Return(userRepositoryMock)

	suite.api, _ = NewApi(
		echo.New(),
		logger,
		repositoryMock,
		cfg.CorsAllowOrigins,
		cfg.AccessTokenLifetime,
		cfg.RefreshTokenLifetime,
		cfg.AuthTokensSecretKey,
		&config.S3Settings{},
		http.DefaultClient,
		"",
		"",
	)
	suite.zapRecorder = zapRecorder
	suite.logObserver = logger
	suite.user = user
}

func (suite *UserApiTestSuite) TearDownTest() {}

func (suite *UserApiTestSuite) TestUserApi_Login_Ok() {
	body := `{"login": "1234567890", "password": "Password123!"}`
	req := httptest.NewRequest(http.MethodPost, NoAuthGroupPath+"/users/login", strings.NewReader(body))
	req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	suite.api.http.ServeHTTP(rec, req)

	assert.Equal(suite.T(), http.StatusOK, rec.Code)
	assert.NotEmpty(suite.T(), rec.Body.String())

	token := new(Auth)
	err := json.Unmarshal(rec.Body.Bytes(), token)
	assert.NoError(suite.T(), err)
	assert.NotEmpty(suite.T(), token.AccessToken)
	assert.NotEmpty(suite.T(), token.RefreshToken)
}

func (suite *UserApiTestSuite) TestUserApi_Login_BindAndValidate_Error() {
	body := `{"login": "1234567890", "password": "password"}`
	req := httptest.NewRequest(http.MethodPost, NoAuthGroupPath+"/users/login", strings.NewReader(body))
	req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	suite.api.http.ServeHTTP(rec, req)

	assert.Equal(suite.T(), http.StatusBadRequest, rec.Code)
	assert.NotEmpty(suite.T(), rec.Body.String())

	rspErr := new(pkg.Error)
	err := json.Unmarshal(rec.Body.Bytes(), rspErr)
	assert.NoError(suite.T(), err)
	assert.Equal(suite.T(), pkg.ErrorValidationFailed.Code, rspErr.Code)
	assert.Equal(suite.T(), pkg.ErrorValidationFailed.Reason, rspErr.Reason)
	assert.Regexp(suite.T(), "Password", rspErr.Details)
}

func (suite *UserApiTestSuite) TestUserApi_Login_UserRepository_AuthUser_Error() {
	userRepositoryMock := &mocks.UserRepositoryInterface{}
	userRepositoryMock.On("AuthUser", mock.Anything, mock.Anything, mock.Anything).
		Return(nil, errors.New("TestUserApi_Login_BindAndValidate_Error"))
	repositoryMock := &mocks.Repository{}
	repositoryMock.On("GetUserRepository").Return(userRepositoryMock)
	suite.api.repository = repositoryMock

	body := `{"login": "1234567890", "password": "Password123!"}`
	req := httptest.NewRequest(http.MethodPost, NoAuthGroupPath+"/users/login", strings.NewReader(body))
	req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	suite.api.http.ServeHTTP(rec, req)

	assert.Equal(suite.T(), http.StatusUnauthorized, rec.Code)
	assert.Empty(suite.T(), rec.Body.String())
}

func (suite *UserApiTestSuite) TestUserApi_Login_GenerateJwtTokensFn_Error() {
	suite.api.generateJwtTokensFn = func(_ *repository.User) (*Auth, error) {
		return nil, errors.New("TestUserApi_Login_GenerateJwtTokensFn_Error")
	}

	body := `{"login": "1234567890", "password": "Password123!"}`
	req := httptest.NewRequest(http.MethodPost, NoAuthGroupPath+"/users/login", strings.NewReader(body))
	req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	suite.api.http.ServeHTTP(rec, req)

	assert.Equal(suite.T(), http.StatusInternalServerError, rec.Code)
	assert.Empty(suite.T(), rec.Body.String())
}

func (suite *UserApiTestSuite) TestUserApi_RefreshToken_Ok() {
	tokens, err := suite.api.getTokens(suite.user)
	assert.NoError(suite.T(), err)

	body := `{"refresh_token": "` + tokens.RefreshToken + `"}`
	req := httptest.NewRequest(http.MethodPost, NoAuthGroupPath+"/users/refresh", strings.NewReader(body))
	req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	suite.api.http.ServeHTTP(rec, req)

	assert.Equal(suite.T(), http.StatusOK, rec.Code)
	assert.NotEmpty(suite.T(), rec.Body.String())

	tokens = new(Auth)
	err = json.Unmarshal(rec.Body.Bytes(), tokens)
	assert.NoError(suite.T(), err)
	assert.NotEmpty(suite.T(), tokens.AccessToken)
	assert.NotEmpty(suite.T(), tokens.RefreshToken)
}

func (suite *UserApiTestSuite) TestUserApi_RefreshToken_BindAndValidate_Error() {
	body := `{"refresh_token": ""}`
	req := httptest.NewRequest(http.MethodPost, NoAuthGroupPath+"/users/refresh", strings.NewReader(body))
	req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	suite.api.http.ServeHTTP(rec, req)

	assert.Equal(suite.T(), http.StatusBadRequest, rec.Code)
	assert.NotEmpty(suite.T(), rec.Body.String())

	rspErr := new(pkg.Error)
	err := json.Unmarshal(rec.Body.Bytes(), rspErr)
	assert.NoError(suite.T(), err)
	assert.Equal(suite.T(), pkg.ErrorValidationFailed.Code, rspErr.Code)
	assert.Equal(suite.T(), pkg.ErrorValidationFailed.Reason, rspErr.Reason)
	assert.Regexp(suite.T(), "RefreshToken", rspErr.Details)
}

func (suite *UserApiTestSuite) TestUserApi_RefreshToken_JWT_Parse_Error() {
	tokens, err := suite.api.getTokens(suite.user)
	assert.NoError(suite.T(), err)

	suite.api.parseJwtTokenFn = func(_ *jwt.Token) (i interface{}, err error) {
		return nil, errors.New("TestUserApi_RefreshToken_JWT_Parse_Error")
	}

	body := `{"refresh_token": "` + tokens.RefreshToken + `"}`
	req := httptest.NewRequest(http.MethodPost, NoAuthGroupPath+"/users/refresh", strings.NewReader(body))
	req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	suite.api.http.ServeHTTP(rec, req)

	assert.Equal(suite.T(), http.StatusUnauthorized, rec.Code)
	assert.Empty(suite.T(), rec.Body.String())
}

func (suite *UserApiTestSuite) TestUserApi_RefreshToken_Token_Claims_Error() {
	tokens, err := suite.api.getTokens(suite.user)
	assert.NoError(suite.T(), err)

	userRepositoryMock := &mocks.UserRepositoryInterface{}
	userRepositoryMock.On("GetUserById", mock.Anything, mock.Anything).
		Return(nil, errors.New("TestUserApi_RefreshToken_Token_Claims_Error"))
	repositoryMock := &mocks.Repository{}
	repositoryMock.On("GetUserRepository").Return(userRepositoryMock)
	suite.api.repository = repositoryMock

	body := `{"refresh_token": "` + tokens.RefreshToken + `"}`
	req := httptest.NewRequest(http.MethodPost, NoAuthGroupPath+"/users/refresh", strings.NewReader(body))
	req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	suite.api.http.ServeHTTP(rec, req)

	assert.Equal(suite.T(), http.StatusNotFound, rec.Code)
	assert.NotEmpty(suite.T(), rec.Body.String())

	rspErr := new(pkg.Error)
	err = json.Unmarshal(rec.Body.Bytes(), rspErr)
	assert.NoError(suite.T(), err)
	assert.Equal(suite.T(), pkg.ErrorUserNotFound.Code, rspErr.Code)
	assert.Equal(suite.T(), pkg.ErrorUserNotFound.Reason, rspErr.Reason)
	assert.Equal(suite.T(), "TestUserApi_RefreshToken_Token_Claims_Error", rspErr.Details)
}

func (suite *UserApiTestSuite) TestUserApi_RefreshToken_GetTokens_Error() {
	tokens, err := suite.api.getTokens(suite.user)
	assert.NoError(suite.T(), err)

	suite.api.generateJwtTokensFn = func(_ *repository.User) (*Auth, error) {
		return nil, errors.New("TestUserApi_RefreshToken_GetTokens_Error")
	}

	body := `{"refresh_token": "` + tokens.RefreshToken + `"}`
	req := httptest.NewRequest(http.MethodPost, NoAuthGroupPath+"/users/refresh", strings.NewReader(body))
	req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	suite.api.http.ServeHTTP(rec, req)

	assert.Equal(suite.T(), http.StatusInternalServerError, rec.Code)
	assert.NotEmpty(suite.T(), rec.Body.String())

	rspErr := new(pkg.Error)
	err = json.Unmarshal(rec.Body.Bytes(), rspErr)
	assert.NoError(suite.T(), err)
	assert.Equal(suite.T(), pkg.ErrorUnknown.Code, rspErr.Code)
	assert.Equal(suite.T(), pkg.ErrorUnknown.Reason, rspErr.Reason)
	assert.Equal(suite.T(), "TestUserApi_RefreshToken_GetTokens_Error", rspErr.Details)
}
