package api

import (
	"encoding/json"
	"github.com/labstack/echo/v4"
	"gitlab.com/dobrodomik/crm.api/internal/repository"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"io/ioutil"
	"net/http"
	"strconv"
)

const (
	AddressesApiFieldToken      = "token"
	AddressesApiFieldOneString  = "oneString"
	AddressesApiFieldWithParent = "withParent"
	AddressesApiFieldLimit      = "limit"
	AddressesApiFieldQuery      = "query"

	YandexMapApiFieldKey     = "apikey"
	YandexMapFieldFormat     = "format"
	YandexMapApiFieldGeoCode = "geocode"
	YandexMapApiFieldResults = "results"
)

type beneficiary struct {
	*Api
}

type BeneficiaryAddressQuery struct {
	Query string `query:"query"`
}

type BeneficiaryAddressResponse struct {
	Result []BeneficiaryAddressResultItem `json:"result"`
}

type BeneficiaryAddressResultItem struct {
	Zip         int                             `json:"zip"`
	Type        string                          `json:"type"`
	ContentType string                          `json:"contentType"`
	FullName    string                          `json:"fullName"`
	Parents     []BeneficiaryAddressParentsItem `json:"parents"`
}

type BeneficiaryAddressParentsItem struct {
	Name        string `json:"name"`
	Type        string `json:"type"`
	TypeShort   string `json:"typeShort"`
	ContentType string `json:"contentType"`
}

type FindAddressResponseItem struct {
	Zip      int    `json:"zip"`
	FullName string `json:"full_name"`
	City     string `json:"city"`
	District string `json:"district"`
	Label    string `json:"label"`
	Value    string `json:"value"`
}

type YandexMapGeoCoderResponse struct {
	Response *YandexMapGeoCoderResponseResponse `json:"response"`
}

type YandexMapGeoCoderResponseResponse struct {
	GeoObjectCollection *YandexMapGeoCoderResponseGeoObjectCollection `json:"GeoObjectCollection"`
}

type YandexMapGeoCoderResponseGeoObjectCollection struct {
	FeatureMember []*YandexMapGeoCoderResponseFeatureMember `json:"featureMember"`
}

type YandexMapGeoCoderResponseFeatureMember struct {
	GeoObject *YandexMapGeoCoderResponseGeoObject `json:"GeoObject"`
}

type YandexMapGeoCoderResponseGeoObject struct {
	Point *YandexMapGeoCoderResponsePoint `json:"Point"`
}

type YandexMapGeoCoderResponsePoint struct {
	Pos string `json:"pos"`
}

func newBeneficiaryRoute(api *Api) *beneficiary {
	route := &beneficiary{
		Api: api,
	}

	return route
}

func (m *beneficiary) Route(groups *groups) {
	groups.authGroup.POST("/beneficiaries", m.create)
	groups.authGroup.PUT("/beneficiaries/:id", m.update)
	groups.authGroup.GET("/beneficiaries", m.list)
	groups.authGroup.GET("/beneficiaries/:id", m.getById)
	groups.authGroup.DELETE("/beneficiaries/:id", m.delete)
	groups.authGroup.PATCH("/beneficiaries/:id", m.restore)
	groups.authGroup.GET("/beneficiaries/addresses", m.findAddress)
}

func (m *beneficiary) create(c echo.Context) error {
	req := new(repository.Beneficiary)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	req.MapCoords, err = m.findMapCoordsByAddress(req.City + ", " + req.Address)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	result, err := m.repository.GetBeneficiaryRepository().Create(ctx, req)

	if err != nil {
		if err == pkg.ErrorBeneficiaryAlreadyExists {
			return c.JSON(http.StatusConflict, err)
		} else {
			return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
		}
	}

	return c.JSON(http.StatusCreated, result)
}

func (m *beneficiary) update(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		return c.JSON(http.StatusBadRequest, pkg.ErrorInvalidRequest.SetDetails(err.Error()))
	}

	req := new(repository.Beneficiary)
	err = m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	result, err := m.repository.GetBeneficiaryRepository().Update(ctx, uint64(id), req)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, result)
}

func (m *beneficiary) getById(c echo.Context) error {
	req := new(GetByIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	result, err := m.repository.GetBeneficiaryRepository().GetById(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorBeneficiaryNotFound)
	}

	return c.JSON(http.StatusOK, result)
}

func (m *beneficiary) list(c echo.Context) error {
	req := new(repository.BeneficiaryFilter)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	res, err := m.repository.GetBeneficiaryRepository().List(c.Request().Context(), req)

	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, res)
}

func (m *beneficiary) delete(c echo.Context) error {
	req := new(GetByIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	_, err = m.repository.GetBeneficiaryRepository().GetById(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorBeneficiaryNotFound)
	}

	err = m.repository.GetBeneficiaryRepository().Delete(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorBeneficiaryNotFound)
	}

	return c.NoContent(http.StatusOK)
}

func (m *beneficiary) restore(c echo.Context) error {
	req := new(RestoreRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	_, err = m.repository.GetBeneficiaryRepository().GetById(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorBeneficiaryNotFound)
	}

	err = m.repository.GetBeneficiaryRepository().Restore(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorBeneficiaryNotFound)
	}

	return c.NoContent(http.StatusOK)
}

func (m *beneficiary) findAddress(c echo.Context) error {
	req := new(BeneficiaryAddressQuery)
	err := c.Bind(req)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, pkg.ErrorInvalidRequest.SetDetails(err.Error()))
	}

	apiReq, err := http.NewRequest(http.MethodPost, pkg.AddressesApiUrl, nil)

	if err != nil {
		return err
	}

	params := apiReq.URL.Query()
	params.Add(AddressesApiFieldToken, m.addressesApiToken)
	params.Add(AddressesApiFieldOneString, "true")
	params.Add(AddressesApiFieldWithParent, "true")
	params.Add(AddressesApiFieldLimit, "10")
	params.Add(AddressesApiFieldQuery, req.Query)
	apiReq.URL.RawQuery = params.Encode()

	rsp, err := m.httpClient.Do(apiReq)

	if err != nil {
		return err
	}

	if rsp.StatusCode != http.StatusOK {
		return echo.NewHTTPError(http.StatusBadRequest, pkg.ErrorUnknown)
	}

	rspBody, err := ioutil.ReadAll(rsp.Body)
	_ = rsp.Body.Close()

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	msg := new(BeneficiaryAddressResponse)
	err = json.Unmarshal(rspBody, msg)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	items := make([]*FindAddressResponseItem, 0)

	for _, val := range msg.Result {
		item := &FindAddressResponseItem{
			Zip:      val.Zip,
			FullName: val.FullName,
			Label:    val.FullName,
			Value:    val.FullName,
		}

		for _, val1 := range val.Parents {
			if val1.ContentType == "district" || val1.ContentType == "cityOwner" {
				item.District = val1.TypeShort + " " + val1.Name
			}

			if val1.ContentType == "city" {
				item.City = val1.TypeShort + " " + val1.Name
			}
		}

		items = append(items, item)
	}

	return c.JSON(http.StatusOK, items)
}
