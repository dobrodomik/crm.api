package api

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/dobrodomik/crm.api/internal/repository"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"net/http"
	"strconv"
	"time"
)

type order struct {
	*Api
}

type UpdateVolunteerRequest struct {
	Id     uint64               `param:"id" validate:"required,numeric,gt=0"`
	Status string               `json:"status" validate:"required,oneof=in_progress completed"`
	Images repository.JsonSlice `json:"images" validate:"omitempty,dive,url"`
}

func newOrderRoute(api *Api) *order {
	route := &order{
		Api: api,
	}

	return route
}

func (m *order) Route(groups *groups) {
	groups.authGroup.POST("/orders", m.create)
	groups.authGroup.PUT("/orders/:id", m.update)
	groups.authGroup.GET("/orders", m.list)
	groups.authGroup.GET("/orders/:id", m.getById)
	groups.authGroup.DELETE("/orders/:id", m.delete)
	groups.authGroup.PATCH("/orders/:id", m.restore)

	groups.authGroup.PUT("/orders/:id/volunteer", m.updateVolunteer)
	groups.authGroup.DELETE("/orders/:id/volunteer", m.unsetVolunteer)
}

func (m *order) create(c echo.Context) error {
	req := new(repository.Order)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	result, err := m.repository.GetOrderRepository().Create(ctx, req)

	if err != nil {
		if err == pkg.ErrorOrderAlreadyExists {
			return c.JSON(http.StatusConflict, err)
		} else {
			return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
		}
	}

	return c.JSON(http.StatusCreated, result)
}

func (m *order) update(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		return c.JSON(http.StatusBadRequest, pkg.ErrorInvalidRequest.SetDetails(err.Error()))
	}

	req := new(repository.Order)
	err = m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	result, err := m.repository.GetOrderRepository().Update(ctx, uint64(id), req)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, result)
}

func (m *order) getById(c echo.Context) error {
	req := new(GetByIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	result, err := m.repository.GetOrderRepository().GetById(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorBeneficiaryNotFound)
	}

	return c.JSON(http.StatusOK, result)
}

func (m *order) list(c echo.Context) error {
	req := new(repository.OrderFilter)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx, ok := c.(*AuthUserContext)

	if !ok {
		return echo.NewHTTPError(http.StatusForbidden, pkg.ErrorUnknown)
	}

	needSetRegion := true

	for _, val := range ctx.User.Role {
		if val == pkg.RoleAdministrator || val == pkg.RoleOperator {
			needSetRegion = false
		}
	}

	if needSetRegion {
		req.RegionId = ctx.User.RegionId
	}

	res, err := m.repository.GetOrderRepository().List(c.Request().Context(), req)

	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, res)
}

func (m *order) delete(c echo.Context) error {
	req := new(GetByIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	item, err := m.repository.GetOrderRepository().GetById(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorOrderNotFound)
	}

	if item.Status != pkg.OrderStatusNew {
		return c.JSON(http.StatusBadRequest, pkg.ErrorImpossibleDeleteNotNewOrder)
	}

	err = m.repository.GetOrderRepository().Delete(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.NoContent(http.StatusOK)
}

func (m *order) restore(c echo.Context) error {
	req := new(GetByIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	_, err = m.repository.GetBeneficiaryRepository().GetById(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorBeneficiaryNotFound)
	}

	err = m.repository.GetBeneficiaryRepository().Restore(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorBeneficiaryNotFound)
	}

	return c.NoContent(http.StatusOK)
}

func (m *order) updateVolunteer(c echo.Context) error {
	req := new(UpdateVolunteerRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx, ok := c.(*AuthUserContext)

	if !ok {
		return echo.NewHTTPError(http.StatusForbidden, pkg.ErrorUnknown)
	}

	item, err := m.repository.GetOrderRepository().GetById(ctx.Request().Context(), req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorOrderNotFound)
	}

	userId := int(ctx.User.Id)

	if item.Status != pkg.OrderStatusNew && item.VolunteerId != userId {
		return c.JSON(http.StatusForbidden, pkg.ErrorAccessDeniedToNotSelfObject)
	}

	item.Status = req.Status
	item.VolunteerId = userId
	item.Images = req.Images

	if item.Status == pkg.OrderStatusCompleted && item.ReleasedAt == nil {
		t := time.Now()
		item.ReleasedAt = &t
	}

	result, err := m.repository.GetOrderRepository().Update(ctx.Request().Context(), req.Id, item)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, result)
}

func (m *order) unsetVolunteer(c echo.Context) error {
	req := new(GetByIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx, ok := c.(*AuthUserContext)

	if !ok {
		return echo.NewHTTPError(http.StatusForbidden, pkg.ErrorUnknown)
	}

	item, err := m.repository.GetOrderRepository().GetById(ctx.Request().Context(), req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorOrderNotFound)
	}

	userId := int(ctx.User.Id)

	if item.VolunteerId != userId {
		return c.JSON(http.StatusForbidden, pkg.ErrorAccessDeniedToNotSelfObject)
	}

	if item.Status == pkg.OrderStatusCompleted {
		return c.JSON(http.StatusForbidden, pkg.ErrorNotPossibleRejectCompletedOrder)
	}

	item.VolunteerId = 0
	item.Status = pkg.OrderStatusNew
	result, err := m.repository.GetOrderRepository().Update(ctx.Request().Context(), req.Id, item)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, result)
}
