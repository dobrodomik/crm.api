package api

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"gitlab.com/dobrodomik/crm.api/internal/repository"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"net/http"
	"strconv"
)

type user struct {
	*Api
}

type Login struct {
	Login    string `json:"login" validate:"required"`
	Password string `json:"password" validate:"required,password"`
}

type UpdateRequest struct {
	Id uint64 `param:"id" validate:"required,numeric,gt=0"`
}

type RefreshTokenRequest struct {
	RefreshToken string `json:"refresh_token" validate:"required"`
}

func newUserRoute(api *Api) *user {
	route := &user{
		Api: api,
	}

	return route
}

func (m *user) Route(groups *groups) {
	groups.authGroup.POST("/users", m.createUser)
	groups.authGroup.PUT("/users/:id", m.updateUser)
	groups.authGroup.GET("/users", m.listUsers)
	groups.authGroup.GET("/users/:id", m.getUserById)
	groups.authGroup.DELETE("/users/:id", m.deleteUser)
	groups.authGroup.PATCH("/users/:id", m.restoreUser)
	groups.authGroup.GET("/users/me", m.me)

	groups.noAuthGroup.POST("/users/login", m.login)
	groups.noAuthGroup.POST("/users/refresh", m.refreshToken)
}

func (m *user) login(c echo.Context) error {
	req := new(Login)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	user, err := m.repository.GetUserRepository().AuthUser(ctx, req.Login, req.Password)

	if err != nil {
		return c.JSON(http.StatusUnauthorized, pkg.ErrorAuthLoginOrPasswordIncorrect.SetDetails(err.Error()))
	}

	tokens, err := m.generateJwtTokensFn(user)

	if err != nil {
		return c.NoContent(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusOK, tokens)
}

func (m *user) createUser(c echo.Context) error {
	req := new(repository.User)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx, ok := c.(*AuthUserContext)

	if !ok {
		return echo.NewHTTPError(http.StatusForbidden, pkg.ErrorUnknown)
	}

	if ctx.IsRegionCurator {
		req.RegionId = ctx.User.RegionId
		req.Role = []string{pkg.RoleVolunteer}
	} else {
		if req.RegionId == 0 || req.Role == nil {
			return c.JSON(http.StatusBadRequest, pkg.ErrorUserFieldsRoleAndRegionEmpty)
		}
	}

	userObj, err := m.repository.GetUserRepository().CreateUser(ctx.Request().Context(), req)

	if err != nil {
		if err == pkg.ErrorUserAlreadyExists {
			return c.JSON(http.StatusConflict, err)
		} else {
			return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
		}
	}

	return c.JSON(http.StatusCreated, userObj)
}

func (m *user) updateUser(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		return c.JSON(http.StatusBadRequest, pkg.ErrorInvalidRequest.SetDetails(err.Error()))
	}

	req := new(repository.User)
	err = m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx, ok := c.(*AuthUserContext)

	if !ok {
		return echo.NewHTTPError(http.StatusForbidden, pkg.ErrorUnknown)
	}

	if ctx.IsRegionCurator {
		req.RegionId = ctx.User.RegionId

		if id != int(ctx.User.Id) {
			req.Role = []string{pkg.RoleVolunteer}
		}
	} else {
		if req.RegionId == 0 || req.Role == nil {
			return c.JSON(http.StatusBadRequest, pkg.ErrorUserFieldsRoleAndRegionEmpty)
		}
	}

	userObj, err := m.repository.GetUserRepository().UpdateUser(ctx.Request().Context(), uint64(id), req)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, userObj)
}

func (m *user) getUserById(c echo.Context) error {
	req := new(GetByIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx, ok := c.(*AuthUserContext)

	if !ok {
		return echo.NewHTTPError(http.StatusForbidden, pkg.ErrorUnknown)
	}

	needSetRegion := true

	for _, val := range ctx.User.Role {
		if val == pkg.RoleAdministrator || val == pkg.RoleOperator {
			needSetRegion = false
		}
	}

	user, err := m.repository.GetUserRepository().GetUserById(c.Request().Context(), req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	if needSetRegion {
		if user.RegionId != ctx.User.RegionId {
			return c.JSON(http.StatusForbidden, pkg.ErrorUserNotFound)
		}
	}

	return c.JSON(http.StatusOK, user)
}

func (m *user) listUsers(c echo.Context) error {
	req := new(repository.UserFilter)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx, ok := c.(*AuthUserContext)

	if !ok {
		return echo.NewHTTPError(http.StatusForbidden, pkg.ErrorUnknown)
	}

	needSetRegion := true

	for _, val := range ctx.User.Role {
		if val == pkg.RoleAdministrator || val == pkg.RoleOperator {
			needSetRegion = false
		}
	}

	if needSetRegion {
		req.RegionId = ctx.User.RegionId
	}

	res, err := m.repository.GetUserRepository().ListUsers(c.Request().Context(), req)

	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, res)
}

func (m *user) deleteUser(c echo.Context) error {
	req := new(GetByIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	user, err := m.repository.GetUserRepository().GetUserById(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	err = m.repository.GetUserRepository().DeleteUser(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	return c.JSON(http.StatusOK, user)
}

func (m *user) restoreUser(c echo.Context) error {
	req := new(GetByIdRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	user, err := m.repository.GetUserRepository().GetUserById(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	err = m.repository.GetUserRepository().RestoreUser(ctx, req.Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	return c.JSON(http.StatusOK, user)
}

func (m *user) refreshToken(c echo.Context) error {
	req := new(RefreshTokenRequest)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	token, err := jwt.Parse(req.RefreshToken, m.parseJwtTokenFn)

	if err != nil {
		return c.NoContent(http.StatusUnauthorized)
	}

	claims, ok := token.Claims.(jwt.MapClaims)

	if !ok || !token.Valid {
		return c.NoContent(http.StatusUnauthorized)
	}

	ctx := c.Request().Context()
	user, err := m.repository.GetUserRepository().GetUserById(ctx, uint64(claims["sub"].(float64)))

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound.SetDetails(err.Error()))
	}

	newTokenPair, err := m.generateJwtTokensFn(user)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, newTokenPair)
}

func (m *user) me(c echo.Context) error {
	ctx := c.(*AuthUserContext)
	user, err := m.repository.GetUserRepository().GetUserById(ctx.Request().Context(), ctx.GetUser().Id)

	if err != nil {
		return c.JSON(http.StatusNotFound, pkg.ErrorUserNotFound)
	}

	return c.JSON(http.StatusOK, user)
}
