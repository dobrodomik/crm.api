package api

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/dobrodomik/crm.api/internal/repository"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"net/http"
)

type comment struct {
	*Api
}

func newCommentRoute(api *Api) *comment {
	route := &comment{
		Api: api,
	}

	return route
}

func (m *comment) Route(groups *groups) {
	groups.authGroup.POST("/comments", m.create)
	groups.authGroup.GET("/comments", m.list)
}

func (m *comment) create(c echo.Context) error {
	req := new(repository.Comment)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx, ok := c.(*AuthUserContext)

	if !ok {
		return echo.NewHTTPError(http.StatusForbidden, pkg.ErrorUnknown)
	}

	req.AuthorId = int(ctx.User.Id)
	obj, err := m.repository.GetCommentRepository().Create(ctx.Request().Context(), req)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusCreated, obj)
}

func (m *comment) list(c echo.Context) error {
	req := new(repository.CommentFilter)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	res, err := m.repository.GetCommentRepository().List(c.Request().Context(), req)

	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, res)
}
