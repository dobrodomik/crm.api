package api

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/dobrodomik/crm.api/internal/config"
	"gitlab.com/dobrodomik/crm.api/internal/repository"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"io/ioutil"
	"net/http"
	"reflect"
	"time"
	"unicode"
)

const (
	AuthGroupPath   = "/auth/api/v1"
	NoAuthGroupPath = "/api/v1"

	detailsSecureConfigIncorrect = "security configuration for secure route not found"
)

type Api struct {
	http                 *echo.Echo
	logger               *zap.Logger
	repository           repository.Repository
	validate             *validator.Validate
	accessTokenLifetime  time.Duration
	refreshTokenLifetime time.Duration
	tokenSecretKey       string
	s3Settings           *config.S3Settings
	httpClient           *http.Client
	addressesApiToken    string
	yandexMapApiToken    string
	httpScheme           string

	generateJwtTokensFn func(*repository.User) (*Auth, error)
	parseJwtTokenFn     jwt.Keyfunc
}

type AuthUserContext struct {
	echo.Context
	User            *repository.User
	IsRegionCurator bool
	IsAdmin         bool
}

type Route interface {
	Route(groups *groups)
}

type groups struct {
	authGroup   *echo.Group
	noAuthGroup *echo.Group
}

type Auth struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    string `json:"expires_in"`
}

type GetByIdRequest struct {
	Id uint64 `param:"id" validate:"required,numeric,gt=0"`
}

type GetByIdRequestInt64 struct {
	Id int64 `param:"id" validate:"required,numeric,gt=0"`
}

type GetByUuIdRequest struct {
	Uuid string `param:"id" validate:"required,uuid"`
}

type RestoreRequest struct {
	Id      uint64 `param:"id" validate:"required,numeric,gt=0"`
	Deleted bool   `json:"deleted"`
}

type ListingRequestBinder struct{}

func NewApi(
	http *echo.Echo,
	logger *zap.Logger,
	repository repository.Repository,
	allowOrigins []string,
	accessTokenLifetime time.Duration,
	refreshTokenLifetime time.Duration,
	tokenSecretKey string,
	s3Settings *config.S3Settings,
	httpClient *http.Client,
	addressesApiToken string,
	yandexMapApiToken string,
	apiSslEnabled bool,
) (*Api, error) {
	http.HideBanner = true
	http.HidePort = true

	http.Use(middleware.Recover())
	http.Use(middleware.BodyDump(func(ctx echo.Context, req, rsp []byte) {
		logger.Info(
			ctx.Request().URL.String(),
			zap.Any(pkg.LogFieldRequestHeaders, ctx.Request().Header),
			zap.ByteString(pkg.LogFieldRequestBody, req),
			zap.Any(pkg.LogFieldResponseStatus, ctx.Response().Status),
			zap.Any(pkg.LogFieldResponseHeaders, ctx.Response().Header()),
			zap.ByteString(pkg.LogFieldResponseBody, rsp),
		)
	}))
	http.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     allowOrigins,
		AllowCredentials: true,
		AllowHeaders:     []string{"authorization", "content-type"},
		ExposeHeaders:    []string{"authorization", "content-type", "set-cookie", "cookie"},
	}))

	jwtMiddleware := middleware.JWTWithConfig(middleware.JWTConfig{
		SigningMethod: jwt.SigningMethodHS512.Name,
		SigningKey:    []byte(tokenSecretKey),
	})

	api := &Api{
		http:                 http,
		logger:               logger,
		repository:           repository,
		validate:             validator.New(),
		accessTokenLifetime:  accessTokenLifetime,
		refreshTokenLifetime: refreshTokenLifetime,
		tokenSecretKey:       tokenSecretKey,
		s3Settings:           s3Settings,
		httpClient:           httpClient,
		addressesApiToken:    addressesApiToken,
		yandexMapApiToken:    yandexMapApiToken,
		httpScheme:           "http",
	}

	if apiSslEnabled {
		api.httpScheme = "https"
	}

	groups := &groups{
		authGroup:   http.Group(AuthGroupPath, jwtMiddleware, api.SecurityMiddleware),
		noAuthGroup: http.Group(NoAuthGroupPath),
	}
	routes, err := api.getRoutes()

	if err != nil {
		return nil, err
	}

	for _, route := range routes {
		route.Route(groups)
	}

	api.initValidators()
	api.generateJwtTokensFn = api.getTokens
	api.parseJwtTokenFn = func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)

		if !ok {
			return nil, pkg.ErrorUnexpectedSigningMethod
		}

		return []byte(tokenSecretKey), nil
	}

	return api, nil
}

func (m *Api) getRoutes() ([]Route, error) {
	common, err := newCommonRoute(m)

	if err != nil {
		return nil, err
	}

	routes := []Route{
		newOrderTypesRoute(m),
		newOrderRoute(m),
		newBeneficiaryRoute(m),
		newNewsRoute(m),
		newPartnerRoute(m),
		newProjectRoute(m),
		newRegionRoute(m),
		newServiceRoute(m),
		newUserRoute(m),
		newCommentRoute(m),
		common,
	}

	return routes, nil
}

func (m *Api) BindAndValidate(ctx echo.Context, receiver interface{}) error {
	err := ctx.Bind(receiver)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, pkg.ErrorInvalidRequest.SetDetails(err.Error()))
	}

	err = m.validate.Struct(receiver)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, pkg.ErrorValidationFailed.SetDetails(m.getValidationError(err)))
	}

	c, ok := ctx.(*AuthUserContext)

	if !ok {
		return nil
	}

	needSetRegion := true

	for _, val := range c.User.Role {
		if val == pkg.RoleAdministrator || val == pkg.RoleOperator {
			needSetRegion = false
		}
	}

	if needSetRegion == false {
		return nil
	}

	receiverRef := reflect.ValueOf(receiver)

	if receiverRef.Type().Kind() != reflect.Ptr {
		return echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorObjectReceiverMustBePointer)
	}

	field := receiverRef.Elem().FieldByName("RegionId")

	if !field.IsValid() {
		return nil
	}

	field.SetUint(c.GetUser().RegionId)

	return nil
}

func (m *Api) initValidators() {
	_ = m.validate.RegisterValidation("password", m.PasswordValidator)
	_ = m.validate.RegisterValidation("datetime", m.DateTimeValidator)
	_ = m.validate.RegisterValidation("date", m.DateValidator)
}

func (m *Api) getValidationError(err error) string {
	return validator.ValidationErrors{err.(validator.ValidationErrors)[0]}.Error()
}

func (m *Api) SecurityMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		tokenUser := c.Get("user").(*jwt.Token)
		claims := tokenUser.Claims.(jwt.MapClaims)
		userId := uint64(claims["sub"].(float64))

		user, err := m.repository.GetUserRepository().GetUserById(c.Request().Context(), userId)

		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
		}

		if user.DeletedAt != nil {
			return echo.NewHTTPError(http.StatusForbidden, pkg.ErrorRequestNotAllowedForUser)
		}

		allowForUser := false
		methods, ok := pkg.AccessControl[c.Path()]

		if !ok {
			return echo.NewHTTPError(
				http.StatusForbidden,
				pkg.ErrorRequestNotAllowedForUser.SetDetails(detailsSecureConfigIncorrect),
			)
		}

		routeRoles, ok := methods[c.Request().Method]

		if !ok {
			return echo.NewHTTPError(
				http.StatusForbidden,
				pkg.ErrorRequestNotAllowedForUser.SetDetails(detailsSecureConfigIncorrect),
			)
		}

		for _, userRole := range user.Role {
			if val, ok := routeRoles[userRole]; ok && val {
				allowForUser = true
			}
		}

		if !allowForUser {
			return echo.NewHTTPError(http.StatusForbidden, pkg.ErrorRequestNotAllowedForUser)
		}

		ctx := &AuthUserContext{
			Context: c,
			User:    user,
		}

		for _, val := range user.Role {
			if val == pkg.RoleRegionCurator {
				ctx.IsRegionCurator = true
			}

			if val == pkg.RoleAdministrator {
				ctx.IsAdmin = true
			}
		}

		return next(ctx)
	}
}

func (m *Api) PasswordValidator(fl validator.FieldLevel) bool {
	var (
		upp, low, num, sym bool
		tot                uint8
	)

	for _, char := range fl.Field().String() {
		switch {
		case unicode.IsUpper(char):
			upp = true
			tot++
		case unicode.IsLower(char):
			low = true
			tot++
		case unicode.IsNumber(char):
			num = true
			tot++
		case unicode.IsPunct(char) || unicode.IsSymbol(char):
			sym = true
			tot++
		default:
			return false
		}
	}

	if !upp || !low || !num || !sym || tot < 8 {
		return false
	}

	return true
}

// DateTime validator
func (m *Api) DateTimeValidator(fl validator.FieldLevel) bool {
	_, err := time.Parse(pkg.FilterDatetimeFormat, fl.Field().String())
	return err == nil
}

// Date validator
func (m *Api) DateValidator(fl validator.FieldLevel) bool {
	_, err := time.Parse(pkg.FilterDateFormat, fl.Field().String())
	return err == nil
}

func (m *Api) getTokens(user *repository.User) (*Auth, error) {
	expiresIn := time.Now().Add(m.accessTokenLifetime)
	claims := jwt.MapClaims{
		"sub":  user.Id,
		"name": user.FirstName + " " + user.LastName,
		"exp":  expiresIn.Unix(),
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	accessToken, err := token.SignedString([]byte(m.tokenSecretKey))

	if err != nil {
		m.logger.Error(
			"access token generation failed",
			zap.Error(err),
			zap.Any(pkg.LogFieldPayload, user),
		)
		return nil, err
	}

	claims = jwt.MapClaims{
		"sub": user.Id,
		"exp": time.Now().Add(m.refreshTokenLifetime).Unix(),
	}
	token = jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	refreshToken, err := token.SignedString([]byte(m.tokenSecretKey))

	if err != nil {
		m.logger.Error(
			"refresh token generation failed",
			zap.Error(err),
			zap.Any(pkg.LogFieldPayload, user),
		)
		return nil, err
	}

	tokens := &Auth{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
		ExpiresIn:    expiresIn.Format(time.RFC3339),
	}
	return tokens, nil
}

func (m *AuthUserContext) GetUser() *repository.User {
	return m.User
}

func (m *Api) findMapCoordsByAddress(address string) (string, error) {
	apiReq, err := http.NewRequest(http.MethodGet, pkg.YandexMapApiUrl, nil)

	if err != nil {
		return "", echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	params := apiReq.URL.Query()
	params.Add(YandexMapApiFieldKey, m.yandexMapApiToken)
	params.Add(YandexMapFieldFormat, "json")
	params.Add(YandexMapApiFieldGeoCode, address)
	params.Add(YandexMapApiFieldResults, "1")
	apiReq.URL.RawQuery = params.Encode()

	rsp, err := m.httpClient.Do(apiReq)

	if err != nil {
		return "", echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	if rsp.StatusCode != http.StatusOK {
		return "", echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown)
	}

	rspBody, err := ioutil.ReadAll(rsp.Body)
	_ = rsp.Body.Close()

	if err != nil {
		return "", echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	msg := new(YandexMapGeoCoderResponse)
	err = json.Unmarshal(rspBody, msg)

	if err != nil {
		return "", echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	if msg.Response == nil || msg.Response.GeoObjectCollection == nil ||
		len(msg.Response.GeoObjectCollection.FeatureMember) <= 0 {
		return "", pkg.ErrorAddressCoordsForMapNotFound
	}

	return msg.Response.GeoObjectCollection.FeatureMember[0].GeoObject.Point.Pos, nil
}
