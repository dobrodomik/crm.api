package api

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/dobrodomik/crm.api/internal/repository"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"net/http"
)

type orderTypes struct {
	*Api
}

func newOrderTypesRoute(api *Api) *orderTypes {
	route := &orderTypes{
		Api: api,
	}

	return route
}

func (m *orderTypes) Route(groups *groups) {
	groups.authGroup.POST("/order-types", m.create)
	groups.authGroup.GET("/order-types", m.list)
}

func (m *orderTypes) create(c echo.Context) error {
	req := new(repository.OrderTypes)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	ctx := c.Request().Context()
	result, err := m.repository.GetOrderTypesRepository().Create(ctx, req)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusCreated, result)
}

func (m *orderTypes) list(c echo.Context) error {
	req := new(repository.OrderTypesFilter)
	err := m.BindAndValidate(c, req)

	if err != nil {
		return err
	}

	res, err := m.repository.GetOrderTypesRepository().List(c.Request().Context(), req)

	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.JSON(http.StatusOK, res)
}
