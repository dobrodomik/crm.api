package api

import (
	"bytes"
	"fmt"
	"github.com/disintegration/imaging"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
)

type common struct {
	*Api
	s3Client *minio.Client
}

type UploadMetadata struct {
	Name        string    `json:"name"`
	Extension   string    `json:"extension"`
	ContentType string    `json:"content_type"`
	Size        int64     `json:"size"`
	Object      io.Reader `json:"-"`
}

type UploadResponse struct {
	Url      string          `json:"url"`
	Metadata *UploadMetadata `json:"metadata"`
}

func newCommonRoute(api *Api) (*common, error) {
	s3Opts := &minio.Options{
		Creds:  credentials.NewStaticV4(api.s3Settings.AccessKeyId, api.s3Settings.SecretAccessKey, ""),
		Secure: api.s3Settings.UseSsl,
	}
	s3Client, err := minio.New(api.s3Settings.Endpoint, s3Opts)

	if err != nil {
		return nil, err
	}

	route := &common{
		Api:      api,
		s3Client: s3Client,
	}
	return route, nil
}

func (m *common) Route(groups *groups) {
	groups.authGroup.POST("/upload/documents", m.uploadDocument)
	groups.authGroup.GET("/documents/:name", m.downloadDocument)
	groups.authGroup.POST("/upload/images", m.uploadImages)
}

func (m *common) uploadDocument(c echo.Context) error {
	file, err := c.FormFile("file")

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	metadata, err := m.processUpload(file)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	ctx := c.Request().Context()
	opts := minio.PutObjectOptions{
		ContentType: metadata.ContentType,
	}
	_, err = m.s3Client.PutObject(ctx, m.s3Settings.BucketNameDocuments, metadata.Name, metadata.Object, metadata.Size, opts)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageUploadFileValidationFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldPayload, file.Filename),
		)
		return echo.NewHTTPError(http.StatusBadRequest, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	rsp := &UploadResponse{
		Url:      fmt.Sprintf("%s://%s/auth/api/v1/documents/%s", m.httpScheme, c.Request().Host, metadata.Name),
		Metadata: metadata,
	}
	return c.JSON(http.StatusOK, rsp)
}

func (m *common) uploadImages(c echo.Context) error {
	file, err := c.FormFile("file")

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	metadata, err := m.processUpload(file)

	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	ctx := c.Request().Context()
	opts := minio.PutObjectOptions{
		ContentType: metadata.ContentType,
	}
	_, err = m.s3Client.PutObject(ctx, m.s3Settings.BucketNameImages, metadata.Name, metadata.Object, metadata.Size, opts)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageUploadFileValidationFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldPayload, file.Filename),
		)
		return echo.NewHTTPError(http.StatusBadRequest, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	rsp := &UploadResponse{
		Url:      fmt.Sprintf("%s/%s/%s", m.s3Settings.FrontendUrl, m.s3Settings.BucketNameImages, metadata.Name),
		Metadata: metadata,
	}
	return c.JSON(http.StatusOK, rsp)
}

func (m *common) downloadDocument(c echo.Context) error {
	name := c.Param("name")
	path := os.TempDir() + string(os.PathSeparator) + name
	err := m.s3Client.FGetObject(c.Request().Context(), m.s3Settings.BucketNameDocuments, name, path, minio.GetObjectOptions{})

	if err != nil {
		m.logger.Error(
			pkg.LogMessageUploadFileValidationFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldPayload, name),
		)
		return echo.NewHTTPError(http.StatusBadRequest, pkg.ErrorUnknown.SetDetails(err.Error()))
	}

	return c.Inline(path, name)
}

func (m *common) processUpload(file *multipart.FileHeader) (*UploadMetadata, error) {
	if file.Size > pkg.UploadMaxSize {
		return nil, pkg.ErrorUploadFileGreaterMaxSize
	}

	src, err := file.Open()

	if err != nil {
		m.logger.Error(
			pkg.LogMessageUploadFileValidationFailed,
			zap.Error(err),
		)
		return nil, pkg.ErrorUnknown.SetDetails(err.Error())
	}
	defer src.Close()

	buffer := make([]byte, 512)
	_, err = src.Read(buffer)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageUploadFileValidationFailed,
			zap.Error(err),
		)
		return nil, pkg.ErrorUnknown.SetDetails(err.Error())
	}

	_, err = src.Seek(0, 0)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageUploadFileValidationFailed,
			zap.Error(err),
		)
		return nil, pkg.ErrorUnknown.SetDetails(err.Error())
	}

	imageSrc, err := m.resizeImage(src)

	if err != nil {
		return nil, err
	}

	res := &UploadMetadata{
		Name:        uuid.New().String() + "." + pkg.ResizedImageExtension,
		Extension:   pkg.ResizedImageExtension,
		ContentType: pkg.ResizedImageContentType,
		Size:        int64(len(imageSrc.Bytes())),
		Object:      imageSrc,
	}

	return res, nil
}

func (m *common) resizeImage(reader io.Reader) (*bytes.Buffer, error) {
	buffer, err := ioutil.ReadAll(reader)
	src, err := imaging.Decode(bytes.NewReader(buffer))

	if err != nil {
		return nil, err
	}

	buf := bytes.NewBuffer(nil)
	resizedImage := imaging.Resize(src, pkg.MaxImageWidth, 0, imaging.Lanczos)
	err = imaging.Encode(buf, resizedImage, imaging.JPEG)

	if err != nil {
		return nil, err
	}

	return buf, nil
}
