package internal

import (
	"bytes"
	"context"
	"errors"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo/v4"
	_ "github.com/lib/pq"
	migrate "github.com/rubenv/sql-migrate"
	"gitlab.com/dobrodomik/crm.api/internal/api"
	"gitlab.com/dobrodomik/crm.api/internal/config"
	"gitlab.com/dobrodomik/crm.api/internal/repository"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"io/ioutil"
	"net/http"
	"time"
)

var (
	ErrorDbDsnIncorrect      = errors.New("the database dsn url not configured")
	ErrorHttpPortIncorrect   = errors.New("the port for run HTTP server not configured")
	ErrorS3SettingsIncorrect = errors.New("the S3 minio server settings not configured")
)

type httpTransport struct {
	Transport http.RoundTripper
	logger    *zap.Logger
}

type httpContextKey struct {
	name string
}

type fatalError struct {
	Message string
	Error   error
}

type Application struct {
	http                         *echo.Echo
	db                           *sqlx.DB
	logger                       *zap.Logger
	fatalFn                      func(msg string, fields ...zap.Field)
	options                      *Options
	cfg                          *config.Config
	applicationStartWaitDuration time.Duration
	dbDsn                        *config.DSN
	repository                   repository.Repository
	httpClient                   *http.Client
}

func NewApplication(cfg *config.Config, opts ...Option) (*Application, error) {
	dbDsn, err := config.NewDSN(cfg.DbDsn)

	if err != nil {
		return nil, ErrorDbDsnIncorrect
	}

	if cfg.HttpPort == "" {
		return nil, ErrorHttpPortIncorrect
	}

	if cfg.S3Settings.Endpoint == "" || cfg.S3Settings.AccessKeyId == "" || cfg.S3Settings.SecretAccessKey == "" ||
		cfg.S3Settings.BucketNameDocuments == "" {
		return nil, ErrorS3SettingsIncorrect
	}

	options := &Options{}

	for _, opt := range opts {
		opt(options)
	}

	db, err := sqlx.Connect(dbDsn.Protocol, dbDsn.Dsn)

	if err != nil {
		return nil, err
	}

	app := &Application{
		options:                      options,
		cfg:                          cfg,
		logger:                       options.logger,
		fatalFn:                      options.logger.Fatal,
		http:                         echo.New(),
		db:                           db,
		applicationStartWaitDuration: pkg.DefaultApplicationStartWaitDuration,
		dbDsn:                        dbDsn,
		repository:                   repository.NewRepository(db, options.logger),
		httpClient:                   newHttpClient(options.logger),
	}
	_, err = api.NewApi(
		app.http,
		app.logger,
		app.repository,
		cfg.CorsAllowOrigins,
		cfg.AccessTokenLifetime,
		cfg.RefreshTokenLifetime,
		cfg.AuthTokensSecretKey,
		cfg.S3Settings,
		app.httpClient,
		cfg.KladrApiToken,
		cfg.YandexMapApiToken,
		cfg.ApiSslEnabled,
	)

	if err != nil {
		return nil, err
	}

	return app, nil
}

func (m *Application) Run() error {
	fatalErrors := make(chan fatalError)

	go func() {
		err := m.http.Start(":" + m.cfg.HttpPort)

		if err != nil {
			fatalErrors <- fatalError{Message: "Http server start failed", Error: err}
		}
	}()

	select {
	case <-time.After(m.applicationStartWaitDuration):
		break
	case err := <-fatalErrors:
		m.logger.Error(err.Message, zap.Error(err.Error))
		close(fatalErrors)
		return err.Error
	}

	migrations := &migrate.FileMigrationSource{
		Dir: m.cfg.DBMigrationsPath,
	}
	migrationSet := migrate.MigrationSet{
		TableName: pkg.DBMigrationTableName,
	}
	_, err := migrationSet.Exec(m.db.DB, m.dbDsn.Protocol, migrations, migrate.Up)

	if err != nil {
		return err
	}

	m.logger.Info("[x] Http server started in port " + m.cfg.HttpPort + "...")
	return nil
}

func (m *Application) Stop() {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err := m.http.Shutdown(ctx)

	if err != nil {
		m.fatalFn("[x] Http server shutdown failed", zap.Error(err))
	}

	m.logger.Info("[x] Http server stopped")

	err = m.db.Close()

	if err != nil {
		m.fatalFn("[x] Database connection close failed", zap.Error(err))
	}

	m.logger.Info("[x] Database connection closed")

	err = m.logger.Sync()

	if err != nil {
		m.fatalFn("[x] Logger sync failed", zap.Error(err))
	}

	m.logger.Info("[x] Logger synced")
}

func (m *Application) GetRepository() repository.Repository {
	return m.repository
}

func newHttpClient(logger *zap.Logger) *http.Client {
	if logger == nil {
		return &http.Client{
			Timeout: 3 * time.Second,
		}
	}

	return &http.Client{
		Transport: &httpTransport{
			logger: logger,
		},
	}
}

func (m *httpTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	ctx := context.WithValue(req.Context(), &httpContextKey{name: "httpRequestStart"}, time.Now())
	req = req.WithContext(ctx)

	var reqBody []byte

	if req.Body != nil {
		reqBody, _ = ioutil.ReadAll(req.Body)
	}

	req.Body = ioutil.NopCloser(bytes.NewBuffer(reqBody))
	rsp, err := http.DefaultTransport.RoundTrip(req)

	if err != nil {
		return rsp, err
	}

	var rspBody []byte

	if rsp.Body != nil {
		rspBody, err = ioutil.ReadAll(rsp.Body)

		if err != nil {
			return rsp, err
		}
	}

	rsp.Body = ioutil.NopCloser(bytes.NewBuffer(rspBody))

	m.logger.Info(
		req.URL.String(),
		zap.Any(pkg.LogFieldRequestHeaders, req.Header),
		zap.ByteString(pkg.LogFieldRequestBody, reqBody),
		zap.Int(pkg.LogFieldResponseStatus, rsp.StatusCode),
		zap.Any(pkg.LogFieldResponseHeaders, rsp.Header),
		zap.ByteString(pkg.LogFieldResponseBody, rspBody),
	)

	return rsp, err
}
