package config

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"time"
)

var (
	configDefaults = map[string]interface{}{
		pkg.ConfigCorsAllowOrigins:     []string{"*"},
		pkg.ConfigAccessTokenLifetime:  pkg.DefaultAccessTokenLifetime,
		pkg.ConfigRefreshTokenLifetime: pkg.DefaultRefreshTokenLifetime,
		pkg.ConfigHttpPort:             pkg.DefaultHttpPort,
		pkg.ConfigDBMigrationsPath:     pkg.DefaultDBMigrationsPath,
		pkg.ConfigS3UseSsl:             pkg.DefaultS3UseSsl,
		pkg.ConfigApiSslEnabled:        pkg.DefaultApiSslEnabled,
	}
	configBinds = []string{
		pkg.ConfigDbDsn, pkg.ConfigCorsAllowOrigins, pkg.ConfigAccessTokenLifetime, pkg.ConfigRefreshTokenLifetime,
		pkg.ConfigAuthTokenSecret, pkg.ConfigHttpPort, pkg.ConfigDBMigrationsPath, pkg.ConfigS3Endpoint, pkg.ConfigS3AccessKeyId,
		pkg.ConfigS3SecretAccessKey, pkg.ConfigS3BucketNameDocuments, pkg.ConfigS3UseSsl, pkg.ConfigKladrApiToken,
		pkg.ConfigYandexMapApiToken, pkg.ConfigApiSslEnabled, pkg.ConfigS3BucketNameImages, pkg.ConfigS3FrontendUrl,
	}
)

type S3Settings struct {
	Endpoint            string
	AccessKeyId         string
	SecretAccessKey     string
	BucketNameDocuments string
	BucketNameImages    string
	FrontendUrl         string
	UseSsl              bool
}

type Config struct {
	DbDsn                string
	HttpPort             string
	CorsAllowOrigins     []string
	AccessTokenLifetime  time.Duration
	RefreshTokenLifetime time.Duration
	AuthTokensSecretKey  string
	DBMigrationsPath     string
	KladrApiToken        string
	YandexMapApiToken    string
	ApiSslEnabled        bool

	*S3Settings
}

func NewConfig(cmd *cobra.Command) *Config {
	for k, v := range configDefaults {
		viper.SetDefault(k, v)
	}

	for _, v := range configBinds {
		_ = viper.BindEnv(v)
	}

	if cmd != nil {
		for _, v := range configBinds {
			_ = viper.BindPFlag(v, cmd.Flags().Lookup(v))
		}
	}

	cfg := &Config{
		DbDsn:                viper.GetString(pkg.ConfigDbDsn),
		HttpPort:             viper.GetString(pkg.ConfigHttpPort),
		CorsAllowOrigins:     viper.GetStringSlice(pkg.ConfigCorsAllowOrigins),
		AccessTokenLifetime:  time.Duration(viper.GetInt(pkg.ConfigAccessTokenLifetime)) * time.Second,
		RefreshTokenLifetime: time.Duration(viper.GetInt(pkg.ConfigRefreshTokenLifetime)) * time.Second,
		AuthTokensSecretKey:  viper.GetString(pkg.ConfigAuthTokenSecret),
		DBMigrationsPath:     viper.GetString(pkg.ConfigDBMigrationsPath),
		S3Settings: &S3Settings{
			Endpoint:            viper.GetString(pkg.ConfigS3Endpoint),
			AccessKeyId:         viper.GetString(pkg.ConfigS3AccessKeyId),
			SecretAccessKey:     viper.GetString(pkg.ConfigS3SecretAccessKey),
			BucketNameDocuments: viper.GetString(pkg.ConfigS3BucketNameDocuments),
			BucketNameImages:    viper.GetString(pkg.ConfigS3BucketNameImages),
			FrontendUrl:         viper.GetString(pkg.ConfigS3FrontendUrl),
			UseSsl:              viper.GetBool(pkg.ConfigS3UseSsl),
		},
		KladrApiToken:     viper.GetString(pkg.ConfigKladrApiToken),
		YandexMapApiToken: viper.GetString(pkg.ConfigYandexMapApiToken),
		ApiSslEnabled:     viper.GetBool(pkg.ConfigApiSslEnabled),
	}

	return cfg
}
