package internal

import (
	"gitlab.com/dobrodomik/crm.api/internal/config"
	"go.uber.org/zap"
)

type Options struct {
	logger *zap.Logger
	dbDsn  *config.DSN
}

type Option func(*Options)

func Logger(val *zap.Logger) Option {
	return func(opts *Options) {
		opts.logger = val
	}
}
