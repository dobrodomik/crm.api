package repository

import (
	"context"
	"database/sql"
	"encoding/json"
	"github.com/jmoiron/sqlx"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"strconv"
	"strings"
	"time"
)

type Order struct {
	Model
	BeneficiaryId int        `db:"beneficiary_id" json:"beneficiary_id" validate:"required,numeric,gt=0"`
	TypeId        int        `db:"type_id" json:"type_id" validate:"required,numeric,gt=0"`
	Comment       string     `db:"comment" json:"comment" validate:"omitempty,max=1000"`
	VolunteerId   int        `db:"volunteer_id" json:"volunteer_id"`
	Status        string     `db:"status"`
	Images        JsonSlice  `db:"images" json:"images"`
	ReleasedAt    *time.Time `db:"released_at" json:"released_at"`
}

type OrderListItem struct {
	OrderId            int        `db:"order_id" json:"order_id"`
	OrderCreatedAt     time.Time  `db:"order_created_at" json:"order_created_at"`
	TypeId             int        `db:"type_id" json:"type_id"`
	TypeName           string     `db:"type_name" json:"type_name"`
	Comment            string     `db:"comment" json:"comment"`
	Status             string     `db:"status" json:"status"`
	FullName           string     `db:"full_name" json:"full_name"`
	Birthday           time.Time  `db:"birthday" json:"birthday"`
	RegionId           int        `db:"region_id" json:"region_id"`
	RegionName         string     `db:"region_name" json:"region_name"`
	City               string     `db:"city" json:"city"`
	Address            string     `db:"address" json:"address"`
	Phone              string     `db:"phone" json:"phone"`
	MapCoords          string     `db:"map_coords" json:"map_coords"`
	OrderDeletedAt     *time.Time `db:"order_deleted_at" json:"order_deleted_at"`
	VolunteerId        int        `db:"volunteer_id" json:"volunteer_id"`
	VolunteerFirstName *string    `db:"volunteer_first_name" json:"volunteer_first_name"`
	VolunteerLastName  *string    `db:"volunteer_last_name" json:"volunteer_last_name"`
	BeneficiaryId      int        `db:"beneficiary_id" json:"beneficiary_id"`
	Images             JsonSlice  `db:"order_images" json:"order_images"`
	OrderReleasedAt    *time.Time `db:"order_released_at" json:"order_released_at"`
}

type OrderFilter struct {
	Filter
	BeneficiaryId           int    `query:"beneficiary_id"`
	CreatedAtFrom           string `query:"created_at_from"`
	CreatedAtTo             string `query:"created_at_to"`
	ReleasedAtFrom          string `query:"released_at_from"`
	ReleasedAtTo            string `query:"released_at_to"`
	RegionId                uint64 `query:"region_id"`
	Status                  string `query:"status"`
	TypeId                  *int   `query:"type_id"`
	VolunteerId             int    `query:"volunteer_id"`
	VolunteerName           string `query:"volunteer_name"`
	Deleted                 uint   `query:"deleted"`
	Id                      int    `query:"id"`
	BeneficiaryBirthdayFrom string `query:"beneficiary_birthday_from"`
	BeneficiaryBirthdayTo   string `query:"beneficiary_birthday_to"`
}

type orderRepository struct {
	*repository
}

func (m *OrderListItem) MarshalJSON() ([]byte, error) {
	type Alias OrderListItem
	st := &struct {
		*Alias
		OrderDeletedAt  int64  `json:"order_deleted_at"`
		Birthday        string `json:"birthday"`
		OrderCreatedAt  string `json:"order_created_at"`
		OrderReleasedAt string `json:"order_released_at"`
		Deleted         bool   `json:"deleted"`
	}{
		Alias:          (*Alias)(m),
		Birthday:       m.Birthday.Format("02.01.2006"),
		OrderCreatedAt: m.OrderCreatedAt.Format("02.01.2006 15:04"),
	}

	if m.OrderDeletedAt == nil {
		st.OrderDeletedAt = 0
	} else {
		st.OrderDeletedAt = m.OrderDeletedAt.Unix()
		st.Deleted = true
	}

	if m.OrderReleasedAt != nil {
		st.OrderReleasedAt = m.OrderReleasedAt.Format("02.01.2006 15:04")
	}

	return json.Marshal(st)
}

func newOrderRepository(db *sqlx.DB, logger *zap.Logger) OrderRepositoryInterface {
	rep := &repository{
		db:     db,
		logger: logger,
	}

	inst := &orderRepository{
		repository: rep.readTableFields(&Order{}),
	}

	return inst
}

func (m *orderRepository) GetById(ctx context.Context, id uint64) (*Order, error) {
	result := new(Order)
	query := `SELECT ` + m.repository.tableFieldsString + ` FROM ` + tableOrders + ` WHERE id = $1;`
	args := []interface{}{id}
	err := m.db.GetContext(ctx, result, query, args...)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, pkg.ErrorOrderNotFound
		}

		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return result, nil
}

func (m *orderRepository) List(ctx context.Context, filter *OrderFilter) (*Paginate, error) {
	filterSet := new(FilterSet)
	bindVarIterator := 1

	if filter.Deleted == 0 {
		filterSet.conditions = append(filterSet.conditions, "orders.deleted_at IS NULL")
	} else if filter.Deleted == 1 {
		filterSet.conditions = append(filterSet.conditions, "orders.deleted_at IS NOT NULL")
	}

	if filter.BeneficiaryId != 0 {
		condition := "beneficiary_id = $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.BeneficiaryId)
	}

	if filter.CreatedAtFrom != "" {
		condition := "orders.created_at >= $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.CreatedAtFrom+" 00:00:00")
	}

	if filter.CreatedAtTo != "" {
		condition := "orders.created_at <= $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.CreatedAtTo+" 23:59:59")
	}

	if filter.ReleasedAtFrom != "" {
		condition := "orders.released_at >= $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.ReleasedAtFrom+" 00:00:00")
	}

	if filter.ReleasedAtTo != "" {
		condition := "orders.released_at <= $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.ReleasedAtTo+" 23:59:59")
	}

	if filter.RegionId != 0 {
		condition := "beneficiaries.region_id = $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.RegionId)
	}

	if filter.Status != "" {
		condition := "status = $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.Status)
	}

	if filter.TypeId != nil {
		condition := "type_id = $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, *filter.TypeId)
	}

	if filter.VolunteerId != 0 {
		condition := "volunteer_id = $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.VolunteerId)
	}

	if filter.Id > 0 {
		condition := "orders.id = $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.Id)
	}

	if filter.BeneficiaryBirthdayFrom != "" {
		condition := "beneficiaries.birthday >= $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.BeneficiaryBirthdayFrom)
	}

	if filter.BeneficiaryBirthdayTo != "" {
		condition := "beneficiaries.birthday <= $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.BeneficiaryBirthdayTo)
	}

	if filter.VolunteerName != "" {
		condition := "(users.first_name ILIKE $" + strconv.Itoa(bindVarIterator) + " OR users.last_name ILIKE $" +
			strconv.Itoa(bindVarIterator) + ")"
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, "%"+filter.VolunteerName+"%")
	}

	conditions := ""

	if len(filterSet.conditions) > 0 {
		conditions = " WHERE " + strings.Join(filterSet.conditions, " AND ")
	}

	res := new(Paginate)
	query := `SELECT COUNT(orders.id) FROM orders 
		LEFT JOIN beneficiaries ON orders.beneficiary_id = beneficiaries.id
		LEFT JOIN regions ON beneficiaries.region_id = regions.id
		LEFT JOIN users ON orders.volunteer_id = users.id
		LEFT JOIN types ON orders.type_id = types.id ` + conditions + `;`
	err := m.db.GetContext(ctx, &res.Count, query, filterSet.arguments...)

	if err != nil {
		if err == sql.ErrNoRows {
			return res, nil
		}

		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, filterSet.arguments),
		)
		return nil, err
	}

	if res.Count <= 0 {
		return res, nil
	}

	if filter.Limit <= 0 {
		filter.Limit = pkg.DefaultLimit
	}

	if filter.Offset < 0 {
		filter.Offset = pkg.DefaultOffset
	}

	receiver := make([]*OrderListItem, 0)
	query = `SELECT orders.id AS order_id, orders.created_at AS order_created_at, orders.released_at AS order_released_at, 
			orders.type_id, types.name AS type_name, orders.comment, orders.status, beneficiaries.full_name, beneficiaries.birthday, 
			beneficiaries.region_id, regions.name AS region_name, beneficiaries.city, beneficiaries.address, beneficiaries.phone, 
			beneficiaries.map_coords, beneficiaries.id AS beneficiary_id, orders.deleted_at AS order_deleted_at, orders.volunteer_id, 
			users.first_name AS volunteer_first_name, users.last_name AS volunteer_last_name, orders.images AS order_images
		FROM orders 
		LEFT JOIN beneficiaries ON orders.beneficiary_id = beneficiaries.id
		LEFT JOIN regions ON beneficiaries.region_id = regions.id
		LEFT JOIN users ON orders.volunteer_id = users.id
		LEFT JOIN types ON orders.type_id = types.id ` + conditions + ` ORDER BY orders.id ASC 
		LIMIT ` + strconv.Itoa(filter.Limit) + ` OFFSET ` + strconv.Itoa(filter.Offset) + `;`
	err = m.db.SelectContext(ctx, &receiver, query, filterSet.arguments...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, filterSet.arguments),
		)
		return nil, err
	}

	res.Items = receiver
	return res, nil
}

func (m *orderRepository) Create(ctx context.Context, order *Order) (*Order, error) {
	now := time.Now()
	recordId := uint64(0)
	query := `INSERT INTO ` + tableOrders + ` (beneficiary_id, type_id, comment, created_at, updated_at) 
		VALUES ($1, $2, $3, $4, $5) RETURNING id;`
	args := []interface{}{order.BeneficiaryId, order.TypeId, order.Comment, now, now}
	err := m.db.QueryRowContext(ctx, query, args...).Scan(&recordId)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)

		if m.isUniqueConstraintError(err) {
			return nil, pkg.ErrorOrderAlreadyExists
		}

		return nil, err
	}

	return m.GetById(ctx, recordId)
}

func (m *orderRepository) Update(ctx context.Context, id uint64, data *Order) (*Order, error) {
	query := `UPDATE ` + tableOrders + ` SET type_id = $1, comment = $2, volunteer_id = $3, status = $4, 
		updated_at = $5, images = $6, released_at = $7 WHERE id = $8;`
	args := []interface{}{data.TypeId, data.Comment, data.VolunteerId, data.Status, time.Now(), data.Images,
		data.ReleasedAt, id}
	_, err := m.db.ExecContext(ctx, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return m.GetById(ctx, id)
}

func (m *orderRepository) Delete(ctx context.Context, id uint64) error {
	now := time.Now()
	return m.setDeleted(ctx, tableOrders, id, &now)
}

func (m *orderRepository) Restore(ctx context.Context, id uint64) error {
	return m.setDeleted(ctx, tableOrders, id, nil)
}
