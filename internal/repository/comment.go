package repository

import (
	"context"
	"database/sql"
	"encoding/json"
	"github.com/jmoiron/sqlx"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"strconv"
	"time"
)

type Comment struct {
	Model
	OrderId  int    `db:"order_id" json:"order_id" validate:"required,numeric,gt=0"`
	AuthorId int    `db:"author_id" json:"-"`
	Message  string `db:"message" json:"message" validate:"required,max=2000"`
}

type CommentListItem struct {
	CommentId        int             `db:"comment_id" json:"comment_id"`
	CommentCreatedAt time.Time       `db:"comment_created_at"`
	AuthorId         int             `db:"author_id" json:"author_id"`
	AuthorFirstName  string          `db:"author_first_name"`
	AuthorLastName   string          `db:"author_last_name"`
	AuthorRole       JsonStringSlice `db:"author_role"`
	OrderId          int             `db:"order_id" json:"order_id"`
	Message          string          `db:"message" json:"message"`
}

type CommentFilter struct {
	Filter
	OrderId int `query:"order_id" validate:"required,numeric,gt=0"`
}

type commentRepository struct {
	*repository
}

func (m *CommentListItem) MarshalJSON() ([]byte, error) {
	type Alias CommentListItem
	st := &struct {
		*Alias
		AuthorName       string `json:"author_name"`
		AuthorRole       string `json:"author_role"`
		CommentCreatedAt string `json:"comment_created_at"`
	}{
		Alias:            (*Alias)(m),
		AuthorName:       m.AuthorFirstName + " " + m.AuthorLastName,
		AuthorRole:       m.AuthorRole[0],
		CommentCreatedAt: m.CommentCreatedAt.Format("02.01.2006 15:04"),
	}

	return json.Marshal(st)
}

func newCommentRepository(db *sqlx.DB, logger *zap.Logger) CommentRepositoryInterface {
	rep := &repository{
		db:     db,
		logger: logger,
	}
	inst := &commentRepository{
		repository: rep,
	}

	return inst
}

func (m *commentRepository) List(ctx context.Context, filter *CommentFilter) (*Paginate, error) {
	res := new(Paginate)
	conditions := " WHERE comments.deleted_at IS NULL AND comments.order_id = $1"
	args := []interface{}{filter.OrderId}

	query := `SELECT COUNT(comments.id) FROM ` + tableComments + conditions + `;`
	err := m.db.GetContext(ctx, &res.Count, query, args...)

	if err != nil {
		if err == sql.ErrNoRows {
			return res, nil
		}

		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	if res.Count <= 0 {
		return res, nil
	}

	if filter.Limit <= 0 {
		filter.Limit = pkg.DefaultLimit
	}

	if filter.Offset < 0 {
		filter.Offset = pkg.DefaultOffset
	}

	receiver := make([]*CommentListItem, 0)
	query = `SELECT comments.id AS comment_id, comments.created_at AS comment_created_at, comments.author_id, 
			users.first_name AS author_first_name, users.last_name AS author_last_name, users.role AS author_role,
			comments.order_id, comments.message
		FROM comments 
		LEFT JOIN users ON comments.author_id = users.id` + conditions + ` ORDER BY comments.id ASC 
		LIMIT ` + strconv.Itoa(filter.Limit) + ` OFFSET ` + strconv.Itoa(filter.Offset) + `;`
	err = m.db.SelectContext(ctx, &receiver, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	res.Items = receiver
	return res, nil
}

func (m *commentRepository) GetById(ctx context.Context, id uint64) (*CommentListItem, error) {
	result := new(CommentListItem)
	query := `SELECT comments.id AS comment_id, comments.created_at AS comment_created_at, comments.author_id, 
			users.first_name AS author_first_name, users.last_name AS author_last_name, users.role AS author_role,
			comments.order_id 
		FROM comments 
		LEFT JOIN users ON comments.author_id = users.id WHERE comments.id = $1;`
	args := []interface{}{id}
	err := m.db.GetContext(ctx, result, query, args...)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, pkg.ErrorCommentNotFound
		}

		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return result, nil
}

func (m *commentRepository) Create(ctx context.Context, comment *Comment) (*CommentListItem, error) {
	now := time.Now()
	recordId := uint64(0)
	query := `INSERT INTO ` + tableComments + ` (order_id, author_id, message, created_at, updated_at) 
		VALUES ($1, $2, $3, $4, $5) RETURNING id;`
	args := []interface{}{comment.OrderId, comment.AuthorId, comment.Message, now, now}
	err := m.db.QueryRowContext(ctx, query, args...).Scan(&recordId)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)

		if m.isUniqueConstraintError(err) {
			return nil, pkg.ErrorOrderAlreadyExists
		}

		return nil, err
	}

	return m.GetById(ctx, recordId)
}
