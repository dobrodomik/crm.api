package repository

import (
	"context"
	"database/sql"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"strconv"
	"time"
)

const (
	defaultPartnerPosition = 999
)

type Partner struct {
	Model
	Uuid        string `db:"uuid" json:"-"`
	Name        string `db:"name" json:"name" validate:"required,max=255"`
	Description string `db:"description" json:"description" validate:"required"`
	SiteUrl     string `db:"site_url" json:"site_url" validate:"omitempty,url"`
	Logo        string `db:"logo" json:"logo" validate:"required,url"`
	Position    int    `db:"position" json:"-"`
}

type PartnerFilter struct {
	Filter
	Name    string `query:"name"`
	SiteUrl string `query:"site_url"`
	Deleted uint   `query:"deleted"`
}

type partnerRepository struct {
	*repository
}

func newPartnerRepository(db *sqlx.DB, logger *zap.Logger) PartnerRepositoryInterface {
	rep := &repository{
		db:     db,
		logger: logger,
	}

	inst := &partnerRepository{
		repository: rep.readTableFields(&Partner{}),
	}
	return inst
}

func (m *partnerRepository) CreatePartner(ctx context.Context, partner *Partner) (*Partner, error) {
	now := time.Now()
	uId := uuid.New().String()

	query := "INSERT INTO " + tablePartners + " (uuid, name, description, site_url, logo, position, created_at, updated_at) " +
		"VALUES ($1, $2, $3, $4, $5, $6, $7, $8);"
	args := []interface{}{uId, partner.Name, partner.Description, partner.SiteUrl, partner.Logo,
		defaultPartnerPosition, now, now}
	_, err := m.db.ExecContext(ctx, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return m.GetPartnerByUuid(ctx, uId)
}

func (m *partnerRepository) UpdatePartner(ctx context.Context, uuid string, partner *Partner) (*Partner, error) {
	query := `UPDATE ` + tablePartners + ` SET name = $1, description = $2, site_url = $3, logo = $4, updated_at = $5 
		WHERE uuid = $6;`
	args := []interface{}{partner.Name, partner.Description, partner.SiteUrl, partner.Logo, time.Now(), uuid}
	_, err := m.db.ExecContext(ctx, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
	}

	return m.GetPartnerByUuid(ctx, uuid)
}

func (m *partnerRepository) ListPartners(ctx context.Context, filter *PartnerFilter) (*Paginate, error) {
	filterSet := new(FilterSet)
	bindVarIterator := 1

	if filter.Deleted == 0 {
		filterSet.conditions = append(filterSet.conditions, "deleted_at IS NULL")
	} else if filter.Deleted == 1 {
		filterSet.conditions = append(filterSet.conditions, "deleted_at IS NOT NULL")
	}

	if filter.Name != "" {
		condition := "name LIKE '%$" + strconv.Itoa(bindVarIterator) + "%'"
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.Name)
	}

	if filter.SiteUrl != "" {
		condition := "site_url LIKE '%$" + strconv.Itoa(bindVarIterator) + "%'"
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.SiteUrl)
	}

	filterSet.tableName = tablePartners
	filterSet.fieldsList = m.tableFieldsString
	filterSet.limit = filter.Limit
	filterSet.offset = filter.Offset
	filterSet.sort = " ORDER BY position ASC"
	receiver := make([]*Partner, 0)

	return m.list(ctx, filterSet, &receiver)
}

func (m *partnerRepository) GetPartnerByUuid(ctx context.Context, uuid string) (*Partner, error) {
	partner := new(Partner)
	query := `SELECT ` + m.repository.tableFieldsString + ` FROM ` + tablePartners + ` WHERE uuid = $1;`
	args := []interface{}{uuid}
	err := m.db.GetContext(ctx, partner, query, args...)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, pkg.ErrorUserNotFound
		}

		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return partner, nil
}

func (m *partnerRepository) DeletePartners(ctx context.Context, uuid string) error {
	now := time.Now()

	query := `UPDATE ` + tablePartners + ` SET deleted_at = $1, updated_at = $2 WHERE uuid = $3;`
	args := []interface{}{now, now, uuid}
	_, err := m.db.ExecContext(ctx, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
	}

	return err
}
