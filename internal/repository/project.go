package repository

import (
	"context"
	"database/sql"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"strconv"
	"time"
)

type Project struct {
	Model
	Uuid     string `db:"uuid" json:"-"`
	Title    string `db:"title" json:"title" validate:"required,max=255"`
	ImageUrl string `db:"image_url" json:"image_url" validate:"required,url"`
	Body     string `db:"body" json:"body" validate:"required"`
	Position int    `db:"position" json:"position,omitempty"`
}

type ProjectFilter struct {
	Filter
	Title   string `db:"title" json:"title" validate:"required,max=255"`
	Deleted uint   `query:"deleted" validate:"omitempty,oneof=0 1 2"`
}

type projectRepository struct {
	*repository
}

func newProjectRepository(db *sqlx.DB, logger *zap.Logger) ProjectRepositoryInterface {
	rep := &repository{
		db:     db,
		logger: logger,
	}

	inst := &projectRepository{
		repository: rep.readTableFields(&Project{}),
	}
	return inst
}

func (m *projectRepository) CreateProject(ctx context.Context, project *Project) (*Project, error) {
	now := time.Now()
	uuId := uuid.New().String()

	query := "INSERT INTO " + tableProjects + " (uuid, title, image_url, body, position, created_at, updated_at) " +
		"VALUES ($1, $2, $3, $4, $5, $6, $7);"
	args := []interface{}{uuId, project.Title, project.ImageUrl, project.Body, project.Position, now, now}
	_, err := m.db.ExecContext(ctx, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return m.GetProjectByUuid(ctx, uuId)
}

func (m *projectRepository) UpdateProject(ctx context.Context, uuId string, project *Project) (*Project, error) {
	query := `UPDATE ` + tableProjects + ` SET title = $1, image_url = $2, body = $3, position = $4, updated_at = $5 
		WHERE uuid = $6;`
	args := []interface{}{project.Title, project.ImageUrl, project.Body, project.Position, time.Now(), uuId}
	_, err := m.db.ExecContext(ctx, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
	}

	return m.GetProjectByUuid(ctx, uuId)
}

func (m *projectRepository) GetProjectByUuid(ctx context.Context, uuId string) (*Project, error) {
	entity := new(Project)
	query := `SELECT ` + m.repository.tableFieldsString + ` FROM ` + tableProjects + ` WHERE uuid = $1;`
	args := []interface{}{uuId}
	err := m.db.GetContext(ctx, entity, query, args...)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, pkg.ErrorUserNotFound
		}

		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return entity, nil
}

func (m *projectRepository) ListProject(ctx context.Context, filter *ProjectFilter) (*Paginate, error) {
	filterSet := new(FilterSet)
	bindVarIterator := 1

	if filter.Deleted == 0 {
		filterSet.conditions = append(filterSet.conditions, "deleted_at IS NULL")
	} else if filter.Deleted == 1 {
		filterSet.conditions = append(filterSet.conditions, "deleted_at IS NOT NULL")
	}

	if filter.Title != "" {
		condition := "title LIKE '%$" + strconv.Itoa(bindVarIterator) + "%'"
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.Title)
	}

	filterSet.tableName = tableProjects
	filterSet.fieldsList = m.tableFieldsString
	filterSet.limit = filter.Limit
	filterSet.offset = filter.Offset
	filterSet.sort = " ORDER BY position ASC"
	receiver := make([]*Project, 0)

	return m.list(ctx, filterSet, &receiver)
}

func (m *projectRepository) DeleteProject(ctx context.Context, uuId string) error {
	now := time.Now()

	query := `UPDATE ` + tableProjects + ` SET deleted_at = $1, updated_at = $2 WHERE uuid = $3;`
	args := []interface{}{now, now, uuId}
	_, err := m.db.ExecContext(ctx, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
	}

	return err
}
