package repository

import (
	"context"
	"database/sql"
	"encoding/json"
	"github.com/jmoiron/sqlx"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"time"
)

type Region struct {
	Model
	Name         string    `db:"name" json:"name" validate:"required,max=255"`
	CenterCoords JsonSlice `db:"center_coords" json:"-"`
}

type RegionFilter struct {
	Filter
	Deleted uint `query:"deleted"`
}

type regionRepository struct {
	*repository
}

func (m *Region) MarshalJSON() ([]byte, error) {
	type Alias Region
	st := &struct {
		*Alias
		DeletedAt int64 `json:"deleted_at"`
	}{
		Alias: (*Alias)(m),
	}

	if m.DeletedAt == nil {
		st.DeletedAt = 0
	} else {
		st.DeletedAt = m.DeletedAt.Unix()
	}

	return json.Marshal(st)
}

func (m *Region) UnmarshalJSON(data []byte) error {
	type Alias Region
	st := &struct {
		Deleted bool `json:"deleted"`
		*Alias
	}{
		Alias: (*Alias)(m),
	}

	err := json.Unmarshal(data, &st)

	if err != nil {
		return err
	}

	if st.Deleted {
		t := time.Now()
		m.DeletedAt = &t
	} else {
		m.DeletedAt = nil
	}

	return nil
}

func newRegionRepository(db *sqlx.DB, logger *zap.Logger) RegionRepositoryInterface {
	rep := &repository{
		db:     db,
		logger: logger,
	}

	inst := &regionRepository{
		repository: rep.readTableFields(&Region{}),
	}
	return inst
}

func (m *regionRepository) CreateRegion(ctx context.Context, region *Region) (*Region, error) {
	now := time.Now()

	recordId := int64(0)
	query := "INSERT INTO " + tableRegions + " (name, created_at, updated_at, center_coords) " +
		"VALUES ($1, $2, $3, $4) RETURNING id;"
	args := []interface{}{region.Name, now, now, region.CenterCoords}
	err := m.db.QueryRowContext(ctx, query, args...).Scan(&recordId)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return m.GetRegionById(ctx, recordId)
}

func (m *regionRepository) UpdateRegion(ctx context.Context, id int64, region *Region) (*Region, error) {
	query := `UPDATE ` + tableRegions + ` SET name = $1, deleted_at = $2, updated_at = $3 WHERE id = $4;`
	args := []interface{}{region.Name, region.DeletedAt, time.Now(), id}
	_, err := m.db.ExecContext(ctx, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return m.GetRegionById(ctx, id)
}

func (m *regionRepository) GetRegionById(ctx context.Context, id int64) (*Region, error) {
	entity := new(Region)
	query := `SELECT ` + m.repository.tableFieldsString + ` FROM ` + tableRegions + ` WHERE id = $1;`
	args := []interface{}{id}
	err := m.db.GetContext(ctx, entity, query, args...)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, pkg.ErrorUserNotFound
		}

		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return entity, nil
}

func (m *regionRepository) ListRegions(ctx context.Context, filter *RegionFilter) (*Paginate, error) {
	filterSet := new(FilterSet)

	if filter.Deleted == 0 {
		filterSet.conditions = append(filterSet.conditions, "deleted_at IS NULL")
	} else if filter.Deleted == 1 {
		filterSet.conditions = append(filterSet.conditions, "deleted_at IS NOT NULL")
	}

	filterSet.tableName = tableRegions
	filterSet.fieldsList = m.tableFieldsString
	filterSet.limit = filter.Limit
	filterSet.offset = filter.Offset
	receiver := make([]*Region, 0)

	return m.list(ctx, filterSet, &receiver)
}

func (m *regionRepository) DeleteRegion(ctx context.Context, id int64) error {
	now := time.Now()

	query := `UPDATE ` + tableRegions + ` SET deleted_at = $1, updated_at = $2 WHERE id = $3;`
	args := []interface{}{now, now, id}
	_, err := m.db.ExecContext(ctx, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
	}

	return err
}
