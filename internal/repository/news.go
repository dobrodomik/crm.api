package repository

import (
	"context"
	"database/sql"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"strconv"
	"time"
)

type News struct {
	Model
	Uuid                 string    `db:"uuid" json:"-"`
	Title                string    `db:"title" json:"title" validate:"required,max=255"`
	ImageUrl             string    `db:"image_url" json:"image_url" validate:"required,url"`
	Body                 string    `db:"body" json:"body" validate:"required"`
	PublicationStartedAt time.Time `db:"publication_started_at" json:"publication_started_at"`
	Position             int       `db:"position" json:"position,omitempty"`
}

type NewsFilter struct {
	Filter
	Title               string `query:"title" validate:"omitempty,max=255"`
	PublicationDateFrom string `query:"publication_date_from" validate:"omitempty,datetime"`
	PublicationDateTo   string `query:"publication_date_to" validate:"omitempty,datetime"`
	Deleted             uint   `query:"deleted" validate:"omitempty,oneof=0 1 2"`
}

type newsRepository struct {
	*repository
}

func newNewsRepository(db *sqlx.DB, logger *zap.Logger) NewsRepositoryInterface {
	rep := &repository{
		db:     db,
		logger: logger,
	}

	inst := &newsRepository{
		repository: rep.readTableFields(&News{}),
	}
	return inst
}

func (m *newsRepository) CreateNews(ctx context.Context, news *News) (*News, error) {
	now := time.Now()
	uuId := uuid.New().String()

	query := "INSERT INTO " + tableNews + " (uuid, title, image_url, body, publication_started_at, position, created_at, updated_at) " +
		"VALUES ($1, $2, $3, $4, $5, $6, $7, $8);"
	args := []interface{}{uuId, news.Title, news.ImageUrl, news.Body, news.PublicationStartedAt, news.Position, now, now}
	_, err := m.db.ExecContext(ctx, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return m.GetNewsByUuid(ctx, uuId)
}

func (m *newsRepository) UpdateNews(ctx context.Context, uuId string, news *News) (*News, error) {
	query := `UPDATE ` + tableNews + ` SET title = $1, image_url = $2, body = $3, position = $4, publication_started_at = $5, 
		updated_at = $6 WHERE uuid = $7;`
	args := []interface{}{news.Title, news.ImageUrl, news.Body, news.Position, news.PublicationStartedAt, time.Now(), uuId}
	_, err := m.db.ExecContext(ctx, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
	}

	return m.GetNewsByUuid(ctx, uuId)
}

func (m *newsRepository) GetNewsByUuid(ctx context.Context, uuId string) (*News, error) {
	news := new(News)
	query := `SELECT ` + m.repository.tableFieldsString + ` FROM ` + tableNews + ` WHERE uuid = $1;`
	args := []interface{}{uuId}
	err := m.db.GetContext(ctx, news, query, args...)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, pkg.ErrorUserNotFound
		}

		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return news, nil
}

func (m *newsRepository) ListNews(ctx context.Context, filter *NewsFilter) (*Paginate, error) {
	filterSet := new(FilterSet)
	bindVarIterator := 1

	if filter.Deleted == 0 {
		filterSet.conditions = append(filterSet.conditions, "deleted_at IS NULL")
	} else if filter.Deleted == 1 {
		filterSet.conditions = append(filterSet.conditions, "deleted_at IS NOT NULL")
	}

	if filter.Title != "" {
		condition := "title LIKE '%$" + strconv.Itoa(bindVarIterator) + "%'"
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.Title)
	}

	if filter.PublicationDateFrom != "" {
		date, _ := time.Parse(pkg.FilterDatetimeFormat, filter.PublicationDateFrom)

		condition := "publication_started_at >= $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, date)
	}

	if filter.PublicationDateTo != "" {
		date, _ := time.Parse(pkg.FilterDatetimeFormat, filter.PublicationDateTo)

		condition := "publication_started_at <= $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, date)
	}

	filterSet.tableName = tableNews
	filterSet.fieldsList = m.tableFieldsString
	filterSet.limit = filter.Limit
	filterSet.offset = filter.Offset
	filterSet.sort = " ORDER BY position ASC"
	receiver := make([]*News, 0)

	return m.list(ctx, filterSet, &receiver)
}

func (m *newsRepository) DeleteNews(ctx context.Context, uuId string) error {
	now := time.Now()

	query := `UPDATE ` + tableNews + ` SET deleted_at = $1, updated_at = $2 WHERE uuid = $3;`
	args := []interface{}{now, now, uuId}
	_, err := m.db.ExecContext(ctx, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
	}

	return err
}
