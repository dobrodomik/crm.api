package repository

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"errors"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	tableFieldTagName = "db"

	tableUsers         = "users"
	tablePartners      = "partners"
	tableNews          = "news"
	tableProjects      = "projects"
	tableRegions       = "regions"
	tableBeneficiaries = "beneficiaries"
	tableOrders        = "orders"
	tableOrderTypes    = "types"
	tableComments      = "comments"

	uniqueConstraintErrorCode = "23505"
)

type Repository interface {
	GetUserRepository() UserRepositoryInterface
	GetPartnerRepository() PartnerRepositoryInterface
	GetNewsRepository() NewsRepositoryInterface
	GetProjectRepository() ProjectRepositoryInterface
	GetRegionRepository() RegionRepositoryInterface
	GetBeneficiaryRepository() BeneficiaryRepositoryInterface
	GetOrderRepository() OrderRepositoryInterface
	GetOrderTypesRepository() OrderTypesRepositoryInterface
	GetCommentRepository() CommentRepositoryInterface
}

type Repos struct {
	user        UserRepositoryInterface
	partner     PartnerRepositoryInterface
	news        NewsRepositoryInterface
	project     ProjectRepositoryInterface
	region      RegionRepositoryInterface
	beneficiary BeneficiaryRepositoryInterface
	order       OrderRepositoryInterface
	orderTypes  OrderTypesRepositoryInterface
	comment     CommentRepositoryInterface
}

type UserRepositoryInterface interface {
	AuthUser(ctx context.Context, login, password string) (*User, error)
	GetUserById(ctx context.Context, id uint64) (*User, error)
	ListUsers(ctx context.Context, filter *UserFilter) (*Paginate, error)
	CreateUser(ctx context.Context, user *User) (*User, error)
	UpdateUser(ctx context.Context, id uint64, user *User) (*User, error)
	DeleteUser(ctx context.Context, id uint64) error
	RestoreUser(ctx context.Context, id uint64) error
}

type PartnerRepositoryInterface interface {
	CreatePartner(ctx context.Context, partner *Partner) (*Partner, error)
	UpdatePartner(ctx context.Context, uuid string, partner *Partner) (*Partner, error)
	ListPartners(ctx context.Context, filter *PartnerFilter) (*Paginate, error)
	GetPartnerByUuid(ctx context.Context, uuid string) (*Partner, error)
	DeletePartners(ctx context.Context, uuid string) error
}

type ProjectRepositoryInterface interface {
	CreateProject(ctx context.Context, project *Project) (*Project, error)
	UpdateProject(ctx context.Context, uuId string, project *Project) (*Project, error)
	GetProjectByUuid(ctx context.Context, uuId string) (*Project, error)
	ListProject(ctx context.Context, filter *ProjectFilter) (*Paginate, error)
	DeleteProject(ctx context.Context, uuId string) error
}

type NewsRepositoryInterface interface {
	CreateNews(ctx context.Context, news *News) (*News, error)
	UpdateNews(ctx context.Context, uuId string, news *News) (*News, error)
	GetNewsByUuid(ctx context.Context, uuId string) (*News, error)
	ListNews(ctx context.Context, filter *NewsFilter) (*Paginate, error)
	DeleteNews(ctx context.Context, uuId string) error
}

type RegionRepositoryInterface interface {
	CreateRegion(ctx context.Context, region *Region) (*Region, error)
	UpdateRegion(ctx context.Context, id int64, region *Region) (*Region, error)
	GetRegionById(ctx context.Context, id int64) (*Region, error)
	ListRegions(ctx context.Context, filter *RegionFilter) (*Paginate, error)
	DeleteRegion(ctx context.Context, id int64) error
}

type BeneficiaryRepositoryInterface interface {
	GetById(ctx context.Context, id uint64) (*Beneficiary, error)
	List(ctx context.Context, filter *BeneficiaryFilter) (*Paginate, error)
	Create(ctx context.Context, beneficiary *Beneficiary) (*Beneficiary, error)
	Update(ctx context.Context, id uint64, data *Beneficiary) (*Beneficiary, error)
	Delete(ctx context.Context, id uint64) error
	Restore(ctx context.Context, id uint64) error
}

type OrderRepositoryInterface interface {
	GetById(ctx context.Context, id uint64) (*Order, error)
	List(ctx context.Context, filter *OrderFilter) (*Paginate, error)
	Create(ctx context.Context, order *Order) (*Order, error)
	Update(ctx context.Context, id uint64, data *Order) (*Order, error)
	Delete(ctx context.Context, id uint64) error
	Restore(ctx context.Context, id uint64) error
}

type OrderTypesRepositoryInterface interface {
	GetById(ctx context.Context, id uint64) (*OrderTypes, error)
	List(ctx context.Context, filter *OrderTypesFilter) (*Paginate, error)
	Create(ctx context.Context, in *OrderTypes) (*OrderTypes, error)
}

type CommentRepositoryInterface interface {
	List(ctx context.Context, filter *CommentFilter) (*Paginate, error)
	GetById(ctx context.Context, id uint64) (*CommentListItem, error)
	Create(ctx context.Context, comment *Comment) (*CommentListItem, error)
}

type repository struct {
	db                *sqlx.DB
	logger            *zap.Logger
	mx                sync.Mutex
	tableFieldsMap    map[string]bool
	tableFieldsString string
}

type Model struct {
	Id        uint64     `db:"id" json:"id,omitempty"`
	CreatedAt time.Time  `db:"created_at" json:"created_at,omitempty"`
	UpdatedAt time.Time  `db:"updated_at" json:"-"`
	DeletedAt *time.Time `db:"deleted_at" json:"-"`
}

type Filter struct {
	Limit  int `query:"limit"`
	Offset int `query:"offset"`
}

type Paginate struct {
	Count int         `json:"count"`
	Items interface{} `json:"items"`
}

type FilterSet struct {
	conditions []string
	arguments  []interface{}
	sort       string
	limit      int
	offset     int

	tableName  string
	fieldsList string
}

type JsonSlice []interface{}
type JsonStringSlice []string

func (m *JsonSlice) Scan(src interface{}) error {
	source, ok := src.([]byte)
	if !ok {
		return errors.New("type assertion to byte slice failed")
	}

	err := json.Unmarshal(source, m)

	if err != nil {
		return err
	}

	return nil
}

func (m JsonStringSlice) Value() (driver.Value, error) {
	if len(m) <= 0 {
		return []byte("[]"), nil
	}

	return json.Marshal(m)
}

func (m *JsonStringSlice) Scan(src interface{}) error {
	source, ok := src.([]byte)
	if !ok {
		return errors.New("type assertion to byte slice failed")
	}

	err := json.Unmarshal(source, m)

	if err != nil {
		return err
	}

	return nil
}

func (m JsonSlice) Value() (driver.Value, error) {
	if len(m) <= 0 {
		return []byte("[]"), nil
	}

	return json.Marshal(m)
}

func (m *repository) readTableFields(s interface{}) *repository {
	v := reflect.ValueOf(s).Elem()
	tableFieldsMap := m.readFields(v)
	tableFieldsSlice := make([]string, 0)

	for key := range tableFieldsMap {
		tableFieldsSlice = append(tableFieldsSlice, key)
	}

	m.tableFieldsString = strings.Join(tableFieldsSlice, ", ")
	m.tableFieldsMap = tableFieldsMap

	return m
}

func (m *repository) readFields(v reflect.Value) map[string]bool {
	resultMap := make(map[string]bool)

	for i := 0; i < v.NumField(); i++ {
		field := v.Field(i)

		if field.Type().Kind() == reflect.Struct {
			nestedResultMap := make(map[string]bool)

			switch field.Interface().(type) {
			case time.Time:
				fieldName := v.Type().Field(i).Tag.Get(tableFieldTagName)

				if fieldName == "" || fieldName == "-" {
					continue
				}

				nestedResultMap[fieldName] = true
				break
			default:
				nestedResultMap = m.readFields(field)
			}

			if len(nestedResultMap) > 0 {
				for key, val := range nestedResultMap {
					resultMap[key] = val
				}
			}

			continue
		}

		fieldName := v.Type().Field(i).Tag.Get(tableFieldTagName)

		if fieldName == "" || fieldName == "-" {
			continue
		}

		resultMap[fieldName] = true
	}

	return resultMap
}

func (m *repository) list(ctx context.Context, filterSet *FilterSet, receiver interface{}) (*Paginate, error) {
	conditions := ""

	if len(filterSet.conditions) > 0 {
		conditions = " WHERE " + strings.Join(filterSet.conditions, " AND ")
	}

	res := new(Paginate)
	query := `SELECT COUNT(id) FROM ` + filterSet.tableName + conditions + `;`
	err := m.db.GetContext(ctx, &res.Count, query, filterSet.arguments...)

	if err != nil {
		if err == sql.ErrNoRows {
			return res, nil
		}

		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, filterSet.arguments),
		)
		return nil, err
	}

	if res.Count <= 0 {
		return res, nil
	}

	if filterSet.sort == "" {
		filterSet.sort = " ORDER BY id ASC"
	}

	if filterSet.limit <= 0 {
		filterSet.limit = pkg.DefaultLimit
	}

	if filterSet.offset < 0 {
		filterSet.offset = pkg.DefaultOffset
	}

	query = `SELECT ` + filterSet.fieldsList + ` FROM ` + filterSet.tableName + conditions + filterSet.sort +
		" LIMIT " + strconv.Itoa(filterSet.limit) + " OFFSET " + strconv.Itoa(filterSet.offset) + ";"
	err = m.db.Select(receiver, query, filterSet.arguments...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, filterSet.arguments),
		)
		return nil, err
	}

	res.Items = receiver
	return res, nil
}

func (m *repository) isUniqueConstraintError(err error) bool {
	e, ok := err.(*pq.Error)
	return ok && e.Code == uniqueConstraintErrorCode
}

func (m *repository) setDeleted(ctx context.Context, table string, id interface{}, deletedAt *time.Time) error {
	now := time.Now()

	query := `UPDATE ` + table + ` SET deleted_at = $1, updated_at = $2 WHERE id = $3;`
	args := []interface{}{deletedAt, now, id}
	_, err := m.db.ExecContext(ctx, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
	}

	return err
}

func NewRepository(db *sqlx.DB, logger *zap.Logger) Repository {
	inst := &Repos{
		user:        newUserRepository(db, logger),
		partner:     newPartnerRepository(db, logger),
		news:        newNewsRepository(db, logger),
		project:     newProjectRepository(db, logger),
		region:      newRegionRepository(db, logger),
		beneficiary: newBeneficiaryRepository(db, logger),
		order:       newOrderRepository(db, logger),
		orderTypes:  newOrderTypesRepository(db, logger),
		comment:     newCommentRepository(db, logger),
	}

	return inst
}

func (m *Repos) GetUserRepository() UserRepositoryInterface {
	return m.user
}

func (m *Repos) GetPartnerRepository() PartnerRepositoryInterface {
	return m.partner
}

func (m *Repos) GetNewsRepository() NewsRepositoryInterface {
	return m.news
}

func (m *Repos) GetProjectRepository() ProjectRepositoryInterface {
	return m.project
}

func (m *Repos) GetRegionRepository() RegionRepositoryInterface {
	return m.region
}

func (m *Repos) GetBeneficiaryRepository() BeneficiaryRepositoryInterface {
	return m.beneficiary
}

func (m *Repos) GetOrderRepository() OrderRepositoryInterface {
	return m.order
}

func (m *Repos) GetOrderTypesRepository() OrderTypesRepositoryInterface {
	return m.orderTypes
}

func (m *Repos) GetCommentRepository() CommentRepositoryInterface {
	return m.comment
}
