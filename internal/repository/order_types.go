package repository

import (
	"context"
	"database/sql"
	"github.com/jmoiron/sqlx"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"time"
)

type OrderTypes struct {
	Model
	Name string `db:"name" json:"name" validate:"required,max=255"`
}

type orderTypesRepository struct {
	*repository
}

type OrderTypesFilter struct {
	Filter
}

func newOrderTypesRepository(db *sqlx.DB, logger *zap.Logger) OrderTypesRepositoryInterface {
	rep := &repository{
		db:     db,
		logger: logger,
	}

	inst := &orderTypesRepository{
		repository: rep.readTableFields(&OrderTypes{}),
	}

	return inst
}

func (m *orderTypesRepository) GetById(ctx context.Context, id uint64) (*OrderTypes, error) {
	result := new(OrderTypes)
	query := `SELECT ` + m.repository.tableFieldsString + ` FROM ` + tableOrderTypes + ` WHERE id = $1;`
	args := []interface{}{id}
	err := m.db.GetContext(ctx, result, query, args...)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, pkg.ErrorOrderNotFound
		}

		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return result, nil
}

func (m *orderTypesRepository) List(ctx context.Context, filter *OrderTypesFilter) (*Paginate, error) {
	filterSet := new(FilterSet)
	filterSet.conditions = append(filterSet.conditions, "deleted_at IS NULL")
	filterSet.tableName = tableOrderTypes
	filterSet.fieldsList = m.tableFieldsString
	filterSet.limit = filter.Limit
	filterSet.offset = filter.Offset
	receiver := make([]*OrderTypes, 0)

	return m.list(ctx, filterSet, &receiver)
}

func (m *orderTypesRepository) Create(ctx context.Context, in *OrderTypes) (*OrderTypes, error) {
	now := time.Now()
	recordId := uint64(0)
	query := `INSERT INTO ` + tableOrderTypes + ` (name, created_at, updated_at) VALUES ($1, $2, $3) RETURNING id;`
	args := []interface{}{in.Name, now, now}
	err := m.db.QueryRowContext(ctx, query, args...).Scan(&recordId)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)

		return nil, err
	}

	return m.GetById(ctx, recordId)
}
