package repository

import (
	"context"
	"database/sql"
	"encoding/json"
	"github.com/jmoiron/sqlx"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"golang.org/x/crypto/bcrypt"
	"log"
	"strconv"
	"strings"
	"time"
)

const (
	passwordCost = 8
)

type User struct {
	Model
	FirstName          string          `db:"first_name" json:"first_name" validate:"required,max=255"`
	LastName           string          `db:"last_name" json:"last_name" validate:"required,max=255"`
	Birthday           time.Time       `db:"birthday" json:"birthday" validate:"required"`
	RegionId           uint64          `db:"region_id" json:"region_id" validate:"omitempty,numeric,gt=0"`
	Phone              string          `db:"phone" json:"phone" validate:"required,max=100"`
	Passport           string          `db:"passport" json:"passport" validate:"required,max=100"`
	Documents          JsonStringSlice `db:"documents" json:"documents" validate:"required,dive,url"`
	Role               JsonStringSlice `db:"role" json:"role"`
	Password           string          `db:"password" json:"password,omitempty"`
	RegionCenterCoords JsonSlice       `db:"center_coords" json:"center_coords,omitempty"`
}

type UserFilter struct {
	Filter
	FirstName    string `query:"first_name"`
	LastName     string `query:"last_name"`
	BirthdayFrom string `query:"birthday_from"`
	BirthdayTo   string `query:"birthday_to"`
	RegionId     uint64 `query:"region_id"`
	Phone        string `query:"phone"`
	Passport     string `query:"passport"`
	Role         string `query:"role"`
	Deleted      uint   `query:"deleted"`
	Name         string `query:"name"`
}

func (m *User) MarshalJSON() ([]byte, error) {
	type Alias User
	st := &struct {
		*Alias
		Password  string `json:"password,omitempty"`
		DeletedAt int64  `json:"deleted_at"`
		Birthday  string `json:"birthday"`
	}{
		Alias:    (*Alias)(m),
		Password: "",
		Birthday: m.Birthday.Format("02.01.2006"),
	}

	if m.DeletedAt == nil {
		st.DeletedAt = 0
	} else {
		st.DeletedAt = m.DeletedAt.Unix()
	}

	return json.Marshal(st)
}

func (m *User) UnmarshalJSON(data []byte) error {
	type Alias User
	st := &struct {
		Birthday string `json:"birthday"`
		*Alias
	}{
		Alias: (*Alias)(m),
	}

	err := json.Unmarshal(data, &st)

	if err != nil {
		return err
	}

	birthday, err := time.Parse("02.01.2006", st.Birthday)

	if err != nil {
		return err
	}

	m.Birthday = birthday

	return nil
}

type userRepository struct {
	*repository
}

func newUserRepository(db *sqlx.DB, logger *zap.Logger) UserRepositoryInterface {
	rep := &repository{
		db:     db,
		logger: logger,
	}

	inst := &userRepository{
		repository: rep.readTableFields(&User{}),
	}

	return inst
}

func (m *userRepository) AuthUser(ctx context.Context, phone, password string) (*User, error) {
	user := new(User)
	query := `SELECT users.id, users.created_at, users.deleted_at, users.first_name, users.last_name, users.birthday,
		users.region_id, users.phone, users.passport, users.documents, users.role, users.password, regions.center_coords 
		FROM users LEFT JOIN regions ON users.region_id = regions.id WHERE phone = $1;`
	args := []interface{}{phone}
	err := m.db.GetContext(ctx, user, query, args...)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, pkg.ErrorUserNotFound
		}

		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))

	if err != nil {
		m.logger.Error(
			"password encryption error",
			zap.Error(err),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, pkg.ErrorAuthLoginOrPasswordIncorrect
	}

	return user, nil
}

func (m *userRepository) GetUserById(ctx context.Context, id uint64) (*User, error) {
	user := new(User)
	query := `SELECT users.id, users.created_at, users.deleted_at, users.first_name, users.last_name, users.birthday,
		users.region_id, users.phone, users.passport, users.documents, users.role, users.password, regions.center_coords 
		FROM users LEFT JOIN regions ON users.region_id = regions.id WHERE users.id = $1;`
	args := []interface{}{id}
	err := m.db.GetContext(ctx, user, query, args...)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, pkg.ErrorUserNotFound
		}

		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return user, nil
}

func (m *userRepository) ListUsers(ctx context.Context, filter *UserFilter) (*Paginate, error) {
	filterSet := new(FilterSet)
	bindVarIterator := 1

	if filter.Deleted == 0 {
		filterSet.conditions = append(filterSet.conditions, "users.deleted_at IS NULL")
	} else if filter.Deleted == 1 {
		filterSet.conditions = append(filterSet.conditions, "users.deleted_at IS NOT NULL")
	}

	if filter.FirstName != "" {
		condition := "first_name LIKE $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, "%"+filter.FirstName+"%")
	}

	if filter.LastName != "" {
		condition := "last_name LIKE $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, "%"+filter.LastName+"%")
	}

	if filter.BirthdayFrom != "" {
		condition := "birthday >= $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.BirthdayFrom)
	}

	if filter.BirthdayTo != "" {
		condition := "birthday <= $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.BirthdayTo)
	}

	if filter.RegionId != 0 {
		condition := "region_id = $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.RegionId)
	}

	if filter.Phone != "" {
		condition := "phone LIKE $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, "%"+filter.Phone+"%")
	}

	if filter.Passport != "" {
		condition := "passport LIKE $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, "%"+filter.Passport+"%")
	}

	if filter.Role != "" {
		condition := "role::jsonb ? $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.Role)
	}

	if filter.Name != "" {
		condition := "(first_name ILIKE $" + strconv.Itoa(bindVarIterator) + " OR last_name ILIKE $" + strconv.Itoa(bindVarIterator) + ")"
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, "%"+filter.Name+"%")
	}

	conditions := ""

	if len(filterSet.conditions) > 0 {
		conditions = " WHERE " + strings.Join(filterSet.conditions, " AND ")
	}

	res := new(Paginate)
	query := `SELECT COUNT(users.id) FROM users ` + conditions + `;`
	err := m.db.GetContext(ctx, &res.Count, query, filterSet.arguments...)

	if err != nil {
		if err == sql.ErrNoRows {
			return res, nil
		}

		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, filterSet.arguments),
		)
		return nil, err
	}

	if res.Count <= 0 {
		return res, nil
	}

	if filter.Limit <= 0 {
		filter.Limit = pkg.DefaultLimit
	}

	if filter.Offset < 0 {
		filter.Offset = pkg.DefaultOffset
	}

	receiver := make([]*User, 0)
	query = `SELECT users.id, users.created_at, users.deleted_at, users.first_name, users.last_name, users.birthday,
		users.region_id, users.phone, users.passport, users.documents, users.role, users.password, regions.center_coords
		FROM users LEFT JOIN regions ON users.region_id = regions.id ` + conditions + ` ORDER BY users.id ASC 
		LIMIT ` + strconv.Itoa(filter.Limit) + ` OFFSET ` + strconv.Itoa(filter.Offset) + `;`
	err = m.db.SelectContext(ctx, &receiver, query, filterSet.arguments...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, filterSet.arguments),
		)
		return nil, err
	}

	res.Items = receiver
	return res, nil
}

func (m *userRepository) CreateUser(ctx context.Context, user *User) (*User, error) {
	now := time.Now()
	password, err := m.HashPassword(user.Password)

	if err != nil {
		return nil, err
	}

	userId := 0
	query := "INSERT INTO users (first_name, last_name, birthday, region_id, phone, passport, documents, role," +
		"created_at, updated_at, password) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING id;"
	args := []interface{}{user.FirstName, user.LastName, user.Birthday, user.RegionId, user.Phone, user.Passport,
		user.Documents, user.Role, now, now, password}
	err = m.db.QueryRowContext(ctx, query, args...).Scan(&userId)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)

		if m.isUniqueConstraintError(err) {
			return nil, pkg.ErrorUserAlreadyExists
		}

		return nil, err
	}

	return m.GetUserById(ctx, uint64(userId))
}

func (m *userRepository) UpdateUser(ctx context.Context, id uint64, user *User) (*User, error) {
	query := `UPDATE users SET first_name = $1, last_name = $2, birthday = $3, region_id = $4, phone = $5, passport = $6, 
			documents = $7, role = $8, updated_at = $9`
	args := []interface{}{user.FirstName, user.LastName, user.Birthday, user.RegionId, user.Phone, user.Passport,
		user.Documents, user.Role, time.Now()}

	bindVarIterator := 10

	if user.Password != "" {
		password, err := m.HashPassword(user.Password)

		if err != nil {
			return nil, err
		}

		query += `, password = $` + strconv.Itoa(bindVarIterator)
		args = append(args, password)

		bindVarIterator++
	}

	query += " WHERE id = $" + strconv.Itoa(bindVarIterator) + ";"
	args = append(args, id)
	_, err := m.db.ExecContext(ctx, query, args...)

	log.Println(query)
	log.Println(args)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return m.GetUserById(ctx, id)
}

func (m *userRepository) DeleteUser(ctx context.Context, id uint64) error {
	now := time.Now()
	return m.setDeleted(ctx, tableUsers, id, &now)
}

func (m *userRepository) RestoreUser(ctx context.Context, id uint64) error {
	return m.setDeleted(ctx, tableUsers, id, nil)
}

func (m *userRepository) HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), passwordCost)
	return string(bytes), err
}
