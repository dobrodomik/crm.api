package repository

import (
	"context"
	"database/sql"
	"encoding/json"
	"github.com/jmoiron/sqlx"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"strconv"
	"time"
)

type Beneficiary struct {
	Model
	FullName   string    `db:"full_name" json:"full_name" validate:"required,max=255"`
	Birthday   time.Time `db:"birthday" json:"birthday" validate:"required"`
	RegionId   uint64    `db:"region_id" json:"region_id" validate:"required,numeric,gt=0"`
	City       string    `db:"city" json:"city" validate:"required,max=255"`
	Address    string    `db:"address" json:"address" validate:"required,max=2000"`
	District   string    `db:"district" json:"district" validate:"omitempty,max=255"`
	PostalCode string    `db:"postal_code" json:"postal_code" validate:"omitempty,max=10"`
	Phone      string    `db:"phone" json:"phone" validate:"required,max=100"`
	MapCoords  string    `db:"map_coords" json:"-"`
}

type BeneficiaryFilter struct {
	Filter
	FullName     string `query:"full_name"`
	BirthdayFrom string `query:"birthday_from"`
	BirthdayTo   string `query:"birthday_to"`
	RegionId     uint64 `query:"region_id"`
	Phone        string `query:"phone"`
	City         string `query:"city"`
	Address      string `query:"address"`
	District     string `query:"district"`
	PostalCode   string `query:"postal_code"`
	Deleted      uint   `query:"deleted"`
}

func (m *Beneficiary) MarshalJSON() ([]byte, error) {
	type Alias Beneficiary
	st := &struct {
		*Alias
		DeletedAt int64  `json:"deleted_at"`
		Birthday  string `json:"birthday"`
		Deleted   bool   `json:"deleted"`
	}{
		Alias:    (*Alias)(m),
		Birthday: m.Birthday.Format("02.01.2006"),
	}

	if m.DeletedAt == nil {
		st.DeletedAt = 0
	} else {
		st.DeletedAt = m.DeletedAt.Unix()
		st.Deleted = true
	}

	return json.Marshal(st)
}

func (m *Beneficiary) UnmarshalJSON(data []byte) error {
	type Alias Beneficiary
	st := &struct {
		Birthday string `json:"birthday"`
		*Alias
	}{
		Alias: (*Alias)(m),
	}

	err := json.Unmarshal(data, &st)

	if err != nil {
		return err
	}

	birthday, err := time.Parse("02.01.2006", st.Birthday)

	if err != nil {
		return err
	}

	m.Birthday = birthday

	return nil
}

type beneficiaryRepository struct {
	*repository
}

func newBeneficiaryRepository(db *sqlx.DB, logger *zap.Logger) BeneficiaryRepositoryInterface {
	rep := &repository{
		db:     db,
		logger: logger,
	}

	inst := &beneficiaryRepository{
		repository: rep.readTableFields(&Beneficiary{}),
	}

	return inst
}

func (m *beneficiaryRepository) GetById(ctx context.Context, id uint64) (*Beneficiary, error) {
	result := new(Beneficiary)
	query := `SELECT ` + m.repository.tableFieldsString + ` FROM ` + tableBeneficiaries + ` WHERE id = $1;`
	args := []interface{}{id}
	err := m.db.GetContext(ctx, result, query, args...)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, pkg.ErrorBeneficiaryNotFound
		}

		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return result, nil
}

func (m *beneficiaryRepository) List(ctx context.Context, filter *BeneficiaryFilter) (*Paginate, error) {
	filterSet := new(FilterSet)
	bindVarIterator := 1

	if filter.Deleted == 0 {
		filterSet.conditions = append(filterSet.conditions, "deleted_at IS NULL")
	} else if filter.Deleted == 1 {
		filterSet.conditions = append(filterSet.conditions, "deleted_at IS NOT NULL")
	}

	if filter.FullName != "" {
		condition := "full_name LIKE $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, "%"+filter.FullName+"%")
	}

	if filter.City != "" {
		condition := "city LIKE $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, "%"+filter.City+"%")
	}

	if filter.Address != "" {
		condition := "address LIKE $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, "%"+filter.Address+"%")
	}

	if filter.District != "" {
		condition := "district LIKE $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, "%"+filter.District+"%")
	}

	if filter.PostalCode != "" {
		condition := "district = $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.PostalCode)
	}

	if filter.BirthdayFrom != "" {
		condition := "birthday >= $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.BirthdayFrom)
	}

	if filter.BirthdayTo != "" {
		condition := "birthday <= $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.BirthdayTo)
	}

	if filter.RegionId != 0 {
		condition := "region_id = $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, filter.RegionId)
	}

	if filter.Phone != "" {
		condition := "phone LIKE $" + strconv.Itoa(bindVarIterator)
		bindVarIterator++

		filterSet.conditions = append(filterSet.conditions, condition)
		filterSet.arguments = append(filterSet.arguments, "%"+filter.Phone+"%")
	}

	filterSet.tableName = tableBeneficiaries
	filterSet.fieldsList = m.tableFieldsString
	filterSet.limit = filter.Limit
	filterSet.offset = filter.Offset
	receiver := make([]*Beneficiary, 0)

	return m.list(ctx, filterSet, &receiver)
}

func (m *beneficiaryRepository) Create(ctx context.Context, beneficiary *Beneficiary) (*Beneficiary, error) {
	now := time.Now()

	recordId := uint64(0)
	query := "INSERT INTO " + tableBeneficiaries + " (full_name, birthday, region_id, city, address, district, postal_code, " +
		"phone, created_at, updated_at, map_coords) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING id;"
	args := []interface{}{beneficiary.FullName, beneficiary.Birthday, beneficiary.RegionId, beneficiary.City,
		beneficiary.Address, beneficiary.District, beneficiary.PostalCode, beneficiary.Phone, now, now, beneficiary.MapCoords}
	err := m.db.QueryRowContext(ctx, query, args...).Scan(&recordId)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)

		if m.isUniqueConstraintError(err) {
			return nil, pkg.ErrorBeneficiaryAlreadyExists
		}

		return nil, err
	}

	return m.GetById(ctx, recordId)
}

func (m *beneficiaryRepository) Update(ctx context.Context, id uint64, data *Beneficiary) (*Beneficiary, error) {
	query := `UPDATE ` + tableBeneficiaries + ` SET full_name = $1, birthday = $2, region_id = $3, city = $4, 
		address = $5, district = $6, postal_code = $7, phone = $8, updated_at = $9 WHERE id = $10;`
	args := []interface{}{data.FullName, data.Birthday, data.RegionId, data.City, data.Address, data.District,
		data.PostalCode, data.Phone, time.Now(), id}
	_, err := m.db.ExecContext(ctx, query, args...)

	if err != nil {
		m.logger.Error(
			pkg.LogMessageDatabaseQueryFailed,
			zap.Error(err),
			zap.String(pkg.LogFieldDatabaseFilter, query),
			zap.Any(pkg.LogFieldDatabaseArguments, args),
		)
		return nil, err
	}

	return m.GetById(ctx, id)
}

func (m *beneficiaryRepository) Delete(ctx context.Context, id uint64) error {
	now := time.Now()
	return m.setDeleted(ctx, tableBeneficiaries, id, &now)
}

func (m *beneficiaryRepository) Restore(ctx context.Context, id uint64) error {
	return m.setDeleted(ctx, tableBeneficiaries, id, nil)
}
