package pkg

import (
	"time"
)

const (
	AppName = "Dobrodomik.CRM"

	DefaultLimit                        = 10
	DefaultOffset                       = 0
	DefaultApplicationStartWaitDuration = 50 * time.Millisecond

	DBMigrationTableName = "schema_migrations"

	UploadMaxSize = 31457280

	AddressesApiUrl = "https://kladr-api.ru/api.php"
	YandexMapApiUrl = "https://geocode-maps.yandex.ru/1.x/"
)

const (
	FilterDatetimeFormat    = "2006-01-02T15:04:05"
	FilterDateFormat        = "2006-01-02"
	MaxImageWidth           = 1024
	ResizedImageExtension   = "jpeg"
	ResizedImageContentType = "image/jpeg"
)

const (
	ConfigDbDsn                = "db_dsn"
	ConfigCorsAllowOrigins     = "cors_allow_origins"
	ConfigAccessTokenLifetime  = "access_token_lifetime"
	ConfigRefreshTokenLifetime = "refresh_token_lifetime"
	ConfigAuthTokenSecret      = "auth_token_secret"

	// the minio S3 API endpoint
	ConfigS3Endpoint = "s3_endpoint"
	// the minio S3 access key identifier
	ConfigS3AccessKeyId = "s3_access_key_id"
	// the minio S3 secret access key
	ConfigS3SecretAccessKey = "s3_secret_access_key"
	// the minio S3 bucket name to upload user's documents
	ConfigS3BucketNameDocuments = "s3_bucket_name_documents"
	// the minio S3 bucket name to upload user's images
	ConfigS3BucketNameImages = "s3_bucket_name_images"
	// the minio S3 url
	ConfigS3FrontendUrl = "s3_frontend_url"
	// if true than all request to minio API endpoint must be processed over SSL
	ConfigS3UseSsl = "s3_use_ssl"

	// API server HTTP port
	ConfigHttpPort = "http_port"
	// Path to database migrations files
	ConfigDBMigrationsPath = "db_migrations_path"

	// The token to access to the kladr-api.ru service
	ConfigKladrApiToken = "kladr_api_token"

	// The token to access to the yandex map API service
	ConfigYandexMapApiToken = "yandex_map_api_token"

	// If true then returning https to links for uploaded files
	ConfigApiSslEnabled = "api_ssl_enabled"

	DefaultAccessTokenLifetime  = 900
	DefaultRefreshTokenLifetime = 86400
	DefaultHttpPort             = 3000
	DefaultDBMigrationsPath     = "./migrations"
	DefaultS3UseSsl             = false
	DefaultApiSslEnabled        = false
)

const (
	LogMessageDatabaseQueryFailed        = "query to database failed"
	LogMessageUploadFileValidationFailed = "upload file validation failed"

	LogFieldDatabaseFilter    = "query"
	LogFieldDatabaseArguments = "arguments"
	LogFieldPayload           = "payload"
	LogFieldRequestHeaders    = "request_headers"
	LogFieldRequestBody       = "request_body"
	LogFieldResponseStatus    = "response_status"
	LogFieldResponseHeaders   = "response_headers"
	LogFieldResponseBody      = "response_body"
)

const (
	OrderStatusNew       = "new"
	OrderStatusCompleted = "completed"
)
