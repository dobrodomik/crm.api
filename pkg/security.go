package pkg

import "net/http"

const (
	RoleOperator      = "operator"
	RoleRegionCurator = "region_curator"
	RoleVolunteer     = "volunteer"
	RoleAdministrator = "admin"
)

var (
	AccessControl = map[string]map[string]map[string]bool{
		"/auth/api/v1/users": {
			http.MethodPost: {RoleAdministrator: true, RoleRegionCurator: true},
			http.MethodGet:  {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true, RoleVolunteer: true},
		},
		"/auth/api/v1/users/:id": {
			http.MethodGet:    {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true, RoleVolunteer: true},
			http.MethodPut:    {RoleAdministrator: true, RoleRegionCurator: true},
			http.MethodPatch:  {RoleAdministrator: true},
			http.MethodDelete: {RoleAdministrator: true},
		},
		"/auth/api/v1/users/me": {
			http.MethodGet: {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true, RoleVolunteer: true},
		},
		"/auth/api/v1/upload/documents": {
			http.MethodPost: {RoleAdministrator: true, RoleRegionCurator: true},
		},
		"/auth/api/v1/upload/images": {
			http.MethodPost: {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true, RoleVolunteer: true},
		},
		"/auth/api/v1/documents/:name": {
			http.MethodGet: {RoleAdministrator: true, RoleRegionCurator: true},
		},
		"/auth/api/v1/regions": {
			http.MethodPost: {RoleAdministrator: true},
			http.MethodGet:  {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true, RoleVolunteer: true},
		},
		"/auth/api/v1/regions/:id": {
			http.MethodGet: {RoleAdministrator: true},
			http.MethodPut: {RoleAdministrator: true},
		},
		"/auth/api/v1/beneficiaries": {
			http.MethodPost: {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true},
			http.MethodGet:  {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true, RoleVolunteer: true},
		},
		"/auth/api/v1/beneficiaries/:id": {
			http.MethodGet:    {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true, RoleVolunteer: true},
			http.MethodPut:    {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true},
			http.MethodPatch:  {RoleAdministrator: true, RoleRegionCurator: true},
			http.MethodDelete: {RoleAdministrator: true, RoleRegionCurator: true},
		},
		"/auth/api/v1/beneficiaries/addresses": {
			http.MethodGet: {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true},
		},
		"/auth/api/v1/order-types": {
			http.MethodGet: {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true, RoleVolunteer: true},
		},
		"/auth/api/v1/orders": {
			http.MethodPost: {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true},
			http.MethodGet:  {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true, RoleVolunteer: true},
		},
		"/auth/api/v1/orders/:id": {
			http.MethodGet:    {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true, RoleVolunteer: true},
			http.MethodPut:    {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true},
			http.MethodPatch:  {RoleAdministrator: true, RoleRegionCurator: true},
			http.MethodDelete: {RoleAdministrator: true, RoleRegionCurator: true},
		},
		"/auth/api/v1/orders/:id/volunteer": {
			http.MethodPut:    {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true, RoleVolunteer: true},
			http.MethodDelete: {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true, RoleVolunteer: true},
		},
		"/auth/api/v1/comments": {
			http.MethodGet:  {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true, RoleVolunteer: true},
			http.MethodPost: {RoleAdministrator: true, RoleOperator: true, RoleRegionCurator: true, RoleVolunteer: true},
		},
	}
)
