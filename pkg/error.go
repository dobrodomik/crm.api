package pkg

import (
	"fmt"
)

var (
	ErrorInvalidRequest                  = NewError("api000001", "request contain invalid json")
	ErrorValidationFailed                = NewError("api000002", "request validation failed")
	ErrorUserNotFound                    = NewError("api000003", "user not found")
	ErrorUnexpectedSigningMethod         = NewError("api000004", "unexpected signing method")
	ErrorRequestNotAllowedForUser        = NewError("api000005", "request handling not allowed for user")
	ErrorUploadFileGreaterMaxSize        = NewError("api000006", "file for upload must be lower than or equal 5Mb size")
	ErrorAccessDeniedToNotSelfObject     = NewError("api000007", "you can't edit records which don't own to you")
	ErrorAuthLoginOrPasswordIncorrect    = NewError("api000008", "login or password is incorrect")
	ErrorUserAlreadyExists               = NewError("api000009", "user with specified data already registered")
	ErrorBeneficiaryNotFound             = NewError("api000010", "beneficiary not found")
	ErrorOrderNotFound                   = NewError("api000011", "order not found")
	ErrorBeneficiaryAlreadyExists        = NewError("api000012", "beneficiary with specified data already registered")
	ErrorOrderAlreadyExists              = NewError("api000013", "order with specified data already exist")
	ErrorUserFieldsRoleAndRegionEmpty    = NewError("api000014", "request fields with user's role and user's region cant't be empty")
	ErrorObjectReceiverMustBePointer     = NewError("api000015", "object for deserialize income request must be a pointer")
	ErrorAddressCoordsForMapNotFound     = NewError("api000016", "coords for render address into map not found")
	ErrorNotPossibleRejectCompletedOrder = NewError("api000017", "not possible reject completed order")
	ErrorImpossibleDeleteNotNewOrder     = NewError("api000018", "нельзя удалить заявку, которая уже забрана на исполнение волонтером")
	ErrorCommentNotFound                 = NewError("api000019", "комментарий не найден")
	ErrorUnknown                         = NewError("api000099", "unknown error")
)

type Error struct {
	Code    string `json:"code"`
	Reason  string `json:"reason"`
	Details string `json:"details,omitempty"`
}

func NewError(code, reason string) *Error {
	return &Error{Code: code, Reason: reason}
}

func (m *Error) SetDetails(err string) *Error {
	return &Error{Code: m.Code, Reason: m.Reason, Details: err}
}

func (m *Error) Error() string {
	if m.Details == "" {
		return fmt.Sprintf(`{"code": "%s", "reason": "%s"}`, m.Code, m.Reason)
	}

	return fmt.Sprintf(`{"code": "%s", "reason": "%s", "details": "%s"}`, m.Code, m.Reason, m.Details)
}
