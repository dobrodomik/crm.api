package main

import (
	"gitlab.com/dobrodomik/crm.api/cmd"
)

func main() {
	cmd.ExecuteDefault()
}
