FROM golang:1.14-alpine AS builder

RUN apk add bash ca-certificates git

WORKDIR /application

COPY go.mod go.sum ./
RUN go mod download

COPY . ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -o ./bin/dobrodomik-crm-api .

FROM alpine:3.12
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/* && \
    apk add tzdata && ls /usr/share/zoneinfo && cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime && \
    apk del tzdata && date

WORKDIR /application/
COPY --from=builder /application /application
ENTRYPOINT /application/bin/dobrodomik-crm-api
