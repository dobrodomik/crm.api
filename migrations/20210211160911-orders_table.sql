-- +migrate Up
CREATE TABLE orders
(
    id             BIGSERIAL    NOT NULL,
    created_at     TIMESTAMP    NOT NULL,
    updated_at     TIMESTAMP    NOT NULL,
    deleted_at     TIMESTAMP,
    beneficiary_id BIGINT NOT NULL,
    type_id        BIGINT    NOT NULL,
    comment        TEXT NOT NULL DEFAULT ''::TEXT,
    volunteer_id   BIGINT NOT NULL DEFAULT 0,
    status         VARCHAR(255) NOT NULL DEFAULT 'new',
    PRIMARY KEY (id)
);

CREATE INDEX idx_orders_beneficiary_id ON orders (beneficiary_id);
CREATE INDEX idx_orders_status ON orders (status);
CREATE INDEX idx_orders_deleted_at ON orders (deleted_at);
CREATE INDEX idx_orders_type_id ON orders (type_id);
CREATE INDEX idx_orders_volunteer_id ON orders (volunteer_id);
CREATE INDEX idx_orders_volunteer_id_deleted_at ON orders (volunteer_id, deleted_at);
CREATE INDEX idx_orders_created_at ON orders (created_at);
CREATE INDEX idx_orders_beneficiary_id_status_deleted_at ON orders (beneficiary_id, status, deleted_at);

CREATE TABLE types (
    id          BIGSERIAL    NOT NULL,
    created_at  TIMESTAMP    NOT NULL,
    updated_at  TIMESTAMP    NOT NULL,
    deleted_at  TIMESTAMP,
    name        VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE INDEX idx_types_created_at ON types (created_at);
CREATE INDEX idx_types_deleted_at ON types (deleted_at);
CREATE UNIQUE INDEX idx_types_name ON types (name);

-- +migrate Down
DROP INDEX idx_orders_beneficiary_id;
DROP INDEX idx_orders_status;
DROP INDEX idx_orders_deleted_at;
DROP INDEX idx_orders_type_id;
DROP INDEX idx_orders_created_at;
DROP INDEX idx_orders_volunteer_id;
DROP INDEX idx_orders_volunteer_id_deleted_at;
DROP INDEX idx_orders_beneficiary_id_status_deleted_at;
DROP INDEX idx_types_created_at;
DROP INDEX idx_types_deleted_at;
DROP INDEX idx_types_name;

DROP TABLE orders;
DROP TABLE types;