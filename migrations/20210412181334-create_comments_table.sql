-- +migrate Up
CREATE TABLE comments (
    id BIGSERIAL NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    deleted_at TIMESTAMP,
    order_id BIGINT NOT NULL,
    author_id BIGINT NOT NULL,
    message TEXT NOT NULL,
    PRIMARY KEY (id)
);
-- +migrate Down
DROP TABLE comments;