-- +migrate Up
ALTER TABLE orders ADD COLUMN released_at TIMESTAMP;
UPDATE orders SET released_at = updated_at WHERE status = 'completed';

-- +migrate Down
ALTER TABLE orders DROP COLUMN released_at;