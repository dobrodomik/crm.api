-- +migrate Up
ALTER TABLE regions ADD COLUMN center_coords JSONB NOT NULL DEFAULT '[]'::JSONB;
UPDATE regions SET center_coords = '[55.030199,82.920430]'::JSONB WHERE name = 'Новосибирск';
UPDATE regions SET center_coords = '[59.938951,30.315635]'::JSONB WHERE name = 'Санкт-Петербург';
UPDATE regions SET center_coords = '[55.753215,37.622504]'::JSONB WHERE name = 'Москва';
UPDATE regions SET center_coords = '[55.159897,61.402554]'::JSONB WHERE name = 'Челябинск';
-- +migrate Down
ALTER TABLE regions DROP COLUMN center_coords;
