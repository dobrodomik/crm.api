-- +migrate Up
ALTER TABLE orders ADD COLUMN images JSONB NOT NULL DEFAULT '[]'::JSONB;
-- +migrate Down
ALTER TABLE orders DROP COLUMN images;
