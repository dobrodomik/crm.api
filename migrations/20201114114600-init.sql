-- +migrate Up
CREATE TABLE users
(
    id         BIGSERIAL    NOT NULL,
    created_at TIMESTAMP    NOT NULL,
    updated_at TIMESTAMP    NOT NULL,
    deleted_at TIMESTAMP,
    first_name VARCHAR(255) NOT NULL DEFAULT '',
    last_name  VARCHAR(255) NOT NULL DEFAULT '',
    birthday   TIMESTAMP    NOT NULL,
    region_id  INT          NOT NULL DEFAULT 0,
    phone      VARCHAR(100) NOT NULL DEFAULT '',
    passport   VARCHAR(100) NOT NULL DEFAULT '',
    documents  JSONB        NOT NULL DEFAULT '[]'::JSONB,
    role       JSONB        NOT NULL DEFAULT '[]'::JSONB,
    password   BYTEA        NOT NULL,
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX uniq_users_phone ON users (phone);
CREATE UNIQUE INDEX uniq_users_passport ON users (passport);

CREATE TABLE partners
(
    id          BIGSERIAL,
    uuid        VARCHAR(36)  NOT NULL DEFAULT '',
    name        VARCHAR(255) NOT NULL DEFAULT '',
    description TEXT         NOT NULL DEFAULT '',
    site_url    VARCHAR(255) NOT NULL DEFAULT '',
    logo        TEXT         NOT NULL DEFAULT '',
    position    INT          NOT NULL DEFAULT 999,
    created_at  TIMESTAMP    NOT NULL,
    updated_at  TIMESTAMP    NOT NULL,
    deleted_at  TIMESTAMP,
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX uniq_partners_uuid ON partners (uuid);
CREATE INDEX idx_partners_deleted_at ON partners (deleted_at);
CREATE INDEX idx_partners_position ON partners (position);

CREATE TABLE projects
(
    id         BIGSERIAL    NOT NULL,
    created_at TIMESTAMP    NOT NULL,
    updated_at TIMESTAMP    NOT NULL,
    deleted_at TIMESTAMP,
    uuid       VARCHAR(36)  NOT NULL DEFAULT '',
    title      VARCHAR(255) NOT NULL,
    image_url  VARCHAR(255) NOT NULL,
    body       TEXT         NOT NULL DEFAULT ''::TEXT,
    position   INT          NOT NULL DEFAULT 999,
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX uniq_projects_uuid ON projects (uuid);
CREATE INDEX idx_projects_deleted_at ON projects (deleted_at);
CREATE INDEX idx_projects_position ON projects (position);

CREATE TABLE news
(
    id                     BIGSERIAL    NOT NULL,
    created_at             TIMESTAMP    NOT NULL,
    updated_at             TIMESTAMP    NOT NULL,
    deleted_at             TIMESTAMP,
    uuid                   VARCHAR(36)  NOT NULL DEFAULT '',
    title                  VARCHAR(255) NOT NULL,
    image_url              VARCHAR(255) NOT NULL,
    body                   TEXT         NOT NULL DEFAULT ''::TEXT,
    position               INT          NOT NULL DEFAULT 999,
    publication_started_at TIMESTAMP             DEFAULT now(),
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX uniq_news_uuid ON news (uuid);
CREATE INDEX idx_news_publication_started_at_deleted_at ON news (publication_started_at, deleted_at);
CREATE INDEX idx_news_position ON news (position);

CREATE TABLE regions
(
    id         BIGSERIAL    NOT NULL,
    created_at TIMESTAMP    NOT NULL,
    updated_at TIMESTAMP    NOT NULL,
    deleted_at TIMESTAMP,
    name       VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);
CREATE INDEX idx_regions_deleted_at ON projects (deleted_at);

INSERT INTO regions (name, created_at, updated_at)
VALUES ('Санкт-Петербург', NOW(), NOW()),
       ('Москва', NOW(), NOW()),
       ('Новосибирск', NOW(), NOW()),
       ('Челябинск', NOW(), NOW());

-- +migrate Down
DROP INDEX uniq_users_phone;
DROP INDEX uniq_users_passport;
DROP INDEX uniq_partners_uuid;
DROP INDEX idx_partners_deleted_at;
DROP INDEX idx_partners_position;
DROP INDEX uniq_projects_uuid;
DROP INDEX idx_projects_deleted_at;
DROP INDEX idx_projects_position;
DROP INDEX uniq_news_uuid;
DROP INDEX idx_news_publication_started_at_deleted_at;
DROP INDEX idx_news_position;
DROP INDEX idx_regions_deleted_at;

DROP TABLE users;
DROP TABLE partners;
DROP TABLE projects;
DROP TABLE news;
DROP TABLE regions;
