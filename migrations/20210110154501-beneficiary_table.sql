-- +migrate Up
CREATE TABLE beneficiaries
(
    id          BIGSERIAL    NOT NULL,
    created_at  TIMESTAMP    NOT NULL,
    updated_at  TIMESTAMP    NOT NULL,
    deleted_at  TIMESTAMP,
    full_name   VARCHAR(255) NOT NULL DEFAULT '',
    birthday    TIMESTAMP    NOT NULL,
    region_id   INT          NOT NULL DEFAULT 0,
    city        VARCHAR(255) NOT NULL DEFAULT '',
    address     VARCHAR(2000) NOT NULL DEFAULT '',
    district    VARCHAR(255) NOT NULL DEFAULT '',
    postal_code VARCHAR(10) NOT NULL DEFAULT '',
    phone       VARCHAR(100) NOT NULL DEFAULT '',
    map_coords  VARCHAR(255) NOT NULL DEFAULT '',
    PRIMARY KEY (id)
);
CREATE INDEX idx_beneficiaries_region_id_full_name ON beneficiaries (region_id, full_name);
CREATE INDEX idx_beneficiaries_city_full_name ON beneficiaries (city, full_name);
CREATE INDEX idx_beneficiaries_region_id_city_full_name ON beneficiaries (region_id, city, full_name);
CREATE UNIQUE INDEX uniq_beneficiaries_phone ON beneficiaries (phone);

-- +migrate Down
DROP INDEX idx_beneficiaries_region_id_full_name;
DROP INDEX idx_beneficiaries_city_full_name;
DROP INDEX idx_beneficiaries_region_id_city_full_name;
DROP INDEX uniq_beneficiaries_phone;

DROP TABLE beneficiaries;
