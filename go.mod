module gitlab.com/dobrodomik/crm.api

go 1.15

require (
	github.com/InVisionApp/go-health v2.1.0+incompatible // indirect
	github.com/InVisionApp/go-logger v1.0.1 // indirect
	github.com/bxcodec/faker v2.0.1+incompatible // indirect
	github.com/bxcodec/faker/v3 v3.5.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/disintegration/imaging v1.6.2
	github.com/go-playground/validator/v10 v10.4.0
	github.com/google/uuid v1.1.2
	github.com/jmoiron/sqlx v1.2.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/lib/pq v1.8.0
	github.com/minio/minio-go/v7 v7.0.6
	github.com/rubenv/sql-migrate v0.0.0-20200616145509-8d140a17f351
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.4.0
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20201002170205-7f63de1d35b0
)
