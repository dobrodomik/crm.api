package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/dobrodomik/crm.api/internal/repository"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"time"
)

var (
	adminCmdConfigBinds = []string{"first_name", "last_name", "birthday", "phone", "passport", "password"}
)

var adminCmd = &cobra.Command{
	Use:   "admin",
	Short: "Create new administrator",
	Long:  "Create new administrator",
	RunE: func(cmd *cobra.Command, _ []string) error {
		for _, v := range adminCmdConfigBinds {
			_ = viper.BindPFlag(v, cmd.Flags().Lookup(v))
		}

		birthday, err := time.Parse(pkg.FilterDateFormat, viper.GetString("birthday"))

		if err != nil {
			return err
		}

		user := &repository.User{
			FirstName: viper.GetString("first_name"),
			LastName:  viper.GetString("last_name"),
			Birthday:  birthday,
			Phone:     viper.GetString("phone"),
			Passport:  viper.GetString("passport"),
			Role:      repository.JsonStringSlice{pkg.RoleAdministrator},
			Password:  viper.GetString("password"),
			RegionId:  1,
		}
		_, err = app.GetRepository().GetUserRepository().CreateUser(cmd.Context(), user)

		return err
	},
}

func init() {
	adminCmd.Flags().StringP("first_name", "", "", "the user's first name")
	adminCmd.Flags().StringP("last_name", "", "", "the user's last name")
	adminCmd.Flags().StringP("birthday", "", "", "the user's birthday")
	adminCmd.Flags().StringP("phone", "", "", "the user's phone")
	adminCmd.Flags().StringP("passport", "", "", "the user's passport")
	adminCmd.Flags().StringP("password", "", "", "the user's password to access to system")

	_ = adminCmd.MarkFlagRequired("first_name")
	_ = adminCmd.MarkFlagRequired("last_name")
	_ = adminCmd.MarkFlagRequired("birthday")
	_ = adminCmd.MarkFlagRequired("phone")
	_ = adminCmd.MarkFlagRequired("passport")
	_ = adminCmd.MarkFlagRequired("password")
}
