package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/dobrodomik/crm.api/internal"
	"gitlab.com/dobrodomik/crm.api/internal/config"
	"gitlab.com/dobrodomik/crm.api/pkg"
	"go.uber.org/zap"
	"log"
	"os"
	"os/signal"
	"syscall"
)

var (
	app    *internal.Application
	logger *zap.Logger
	err    error
)

var rootCmd = &cobra.Command{
	Use:   "",
	Short: "DobroDomik Russia CMR backend service",
	Long:  "DobroDomik Russia CMR backend service",
	PersistentPreRunE: func(cmd *cobra.Command, _ []string) error {
		opts := []internal.Option{
			internal.Logger(logger),
		}
		app, err = internal.NewApplication(config.NewConfig(cmd), opts...)

		if err != nil {
			return err
		}

		return nil
	},
	RunE: func(_ *cobra.Command, _ []string) error {
		err := app.Run()

		if err != nil {
			return err
		}

		shutdownSignal := make(chan os.Signal, 1)
		signal.Notify(shutdownSignal, syscall.SIGTERM, syscall.SIGINT, syscall.SIGQUIT)
		<-shutdownSignal

		app.Stop()
		return nil
	},
}

func init() {
	viper.SetEnvPrefix("")
	viper.AutomaticEnv()

	rootCmd.PersistentFlags().StringP(pkg.ConfigDbDsn, "", "", "the database DSN URL")
	rootCmd.PersistentFlags().StringP(pkg.ConfigHttpPort, "", "", "the HTTP server port")
	rootCmd.PersistentFlags().StringSliceP(pkg.ConfigCorsAllowOrigins, "", []string{"*"}, "the CORS allow origins list")
	rootCmd.PersistentFlags().IntP(pkg.ConfigAccessTokenLifetime, "", pkg.DefaultAccessTokenLifetime, "the JWT access token lifetime")
	rootCmd.PersistentFlags().IntP(pkg.ConfigRefreshTokenLifetime, "", pkg.DefaultRefreshTokenLifetime, "the JWT refresh token lifetime")
	rootCmd.PersistentFlags().StringP(pkg.ConfigAuthTokenSecret, "", "", "the secret key for sign auth tokens")
	rootCmd.PersistentFlags().StringP(pkg.ConfigDBMigrationsPath, "", pkg.DefaultDBMigrationsPath, "path to database migrations files")
	rootCmd.PersistentFlags().StringP(pkg.ConfigS3Endpoint, "", "", "the minio S3 API endpoint")
	rootCmd.PersistentFlags().StringP(pkg.ConfigS3AccessKeyId, "", "", "the minio S3 access key identifier")
	rootCmd.PersistentFlags().StringP(pkg.ConfigS3SecretAccessKey, "", "", "the minio S3 secret access key")
	rootCmd.PersistentFlags().StringP(pkg.ConfigS3BucketNameDocuments, "", "", "the minio S3 bucket name to upload user's documents")
	rootCmd.PersistentFlags().StringP(pkg.ConfigS3BucketNameImages, "", "", "the minio S3 bucket name to upload user's images")
	rootCmd.PersistentFlags().BoolP(pkg.ConfigS3UseSsl, "", pkg.DefaultS3UseSsl, "if true than all request to minio API endpoint must be processed over SSL")
	rootCmd.PersistentFlags().StringP(pkg.ConfigS3FrontendUrl, "", "", "the minio S3 url")
	rootCmd.PersistentFlags().StringP(pkg.ConfigKladrApiToken, "", "", "the token to access to the kladr-api.ru service")
	rootCmd.PersistentFlags().StringP(pkg.ConfigYandexMapApiToken, "", "", "The token to access to the yandex map API service")
	rootCmd.PersistentFlags().BoolP(pkg.ConfigApiSslEnabled, "", pkg.DefaultApiSslEnabled, "if true then returning https to links for uploaded files")
}

func ExecuteDefault() {
	logger, err = zap.NewProduction()

	if err != nil {
		log.Fatalf("[x] Logger initialization failed with error: %s", err)
	}

	logger.Named(pkg.AppName)

	rootCmd.AddCommand(adminCmd)
	err = rootCmd.Execute()

	if err != nil {
		log.Fatalf("[x] Commad run failed with error: %s", err)
	}
}
